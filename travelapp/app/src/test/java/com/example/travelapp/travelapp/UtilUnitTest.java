package com.example.travelapp.travelapp;

import android.content.Context;

import com.example.travelapp.travelapp.Helper.Util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by danilofink on 27/01/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class UtilUnitTest {

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Mock
    Context mMockContext;


    @Test
    public void stringOrDefault_isCorrect() throws Exception {
        assertEquals(Util.stringOrDefault("hallo", "default"), "hallo");
        assertEquals(Util.stringOrDefault("", "default"), "default");
        assertEquals(Util.stringOrDefault(null, "default"), "default");
    }

    @Test
    public void isEmpty_isCorrect() throws Exception {
        assertEquals(Util.isEmpty("hallo"), false);
        assertEquals(Util.isEmpty(""), true);
        assertEquals(Util.isEmpty(null), true);
    }

    @Test
    public void getSeconds_isCorrect() throws Exception {
        assertEquals(Util.getSeconds(1000), 1);
        assertEquals(Util.getSeconds(0), 0);
        assertEquals(Util.getSeconds(500), 0);
        assertEquals(Util.getSeconds(999), 0);
        assertEquals(Util.getSeconds(1001), 1);
        assertEquals(Util.getSeconds(123505), 123);
        assertEquals(Util.getSeconds(-123904), -123);
    }

    @Test
    public void setSelectedMemberWithId_isCorrect() throws Exception {
        //needs to be tested with instrumented tests, because it depends on the android framework.
    }
}
