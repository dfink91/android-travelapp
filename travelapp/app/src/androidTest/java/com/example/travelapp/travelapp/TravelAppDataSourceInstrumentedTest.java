package com.example.travelapp.travelapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.travelapp.travelapp.Activities.BaseActivities.ScoccoActivity;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by danilofink on 28/01/18.
 */
@RunWith(AndroidJUnit4.class)
public class TravelAppDataSourceInstrumentedTest {

    public Context context;
    public TravelAppDataSource dataSource;

    @Before
    public void setup(){
        context = InstrumentationRegistry.getTargetContext();
        dataSource = new TravelAppDataSource(context);
    }

    @Test
    public void createDataSource_test() throws Exception {
        Assert.assertNotEquals(null, dataSource);
    }

    @Test
    public void insertTrip_test() throws Exception {
        TravelAppDataSource ds = new TravelAppDataSource(context);
        TTrip trip = new TTrip();
        trip.setName("test trip");
        trip = ds.createObj(trip);
        Assert.assertNotEquals(trip.getId(), TTrip.INVALID_ID);
    }
}
