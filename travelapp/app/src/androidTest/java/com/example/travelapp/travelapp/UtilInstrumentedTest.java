package com.example.travelapp.travelapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Helper.Util;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by danilofink on 28/01/18.
 */
@RunWith(AndroidJUnit4.class)
public class UtilInstrumentedTest {

    @Test
    public void setSelectedMemberWithId_isCorrect() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Spinner testSpinner = new Spinner(appContext);
        ArrayList<TMember> members = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            TMember m = new TMember(i + "-" + i);
            m.setId(i);
            members.add(m);
        }

        ArrayAdapter<TMember> adapter = new ArrayAdapter(appContext, android.R.layout.simple_spinner_item, members);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        testSpinner.setAdapter(adapter);

        Util.setSelectedMemberWithId(testSpinner, 0);
        assertEquals(testSpinner.getSelectedItem().getClass(), TMember.class);
        TMember obj = (TMember) testSpinner.getSelectedItem();
        assertEquals(obj.getId(), 0);
        Util.setSelectedMemberWithId(testSpinner, 3);
        assertEquals(testSpinner.getSelectedItem().getClass(), TMember.class);
        obj = (TMember) testSpinner.getSelectedItem();
        assertEquals(obj.getId(), 3);
    }
}
