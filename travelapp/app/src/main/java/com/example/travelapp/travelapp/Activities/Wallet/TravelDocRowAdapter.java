package com.example.travelapp.travelapp.Activities.Wallet;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TTravelDoc;
import com.example.travelapp.travelapp.Data.TTravelDocGroup;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.Helper.UtilExternal;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 07/01/18.
 */
public class TravelDocRowAdapter extends ArrayAdapter {

    private Context context;
    private TravelAppDataSource dataSource;
    private ArrayList<TTravelDoc> data;
    private static LayoutInflater inflater = null;

    public TravelDocRowAdapter(Context context, ArrayList<TTravelDoc> data) {
        super(context, R.layout.row_travel_doc, data);
        this.context = context;
        dataSource = new TravelAppDataSource(context);
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.row_travel_doc, null);
        TextView txtTitle = (TextView) vi.findViewById(R.id.txt_title);
        TextView txtInfo = (TextView) vi.findViewById(R.id.txt_info);
        TTravelDoc tDoc = data.get(position);
        txtTitle.setText(tDoc.getTitle());
        TMember m = tDoc.getMember(dataSource);
        txtInfo.setText(m != null ? m.getName() : "");
        ImageView imgDoc = (ImageView) vi.findViewById(R.id.img_view_preview);
        if (!Util.isEmpty(tDoc.getFilelink())) {
            UtilExternal.setImageViewWithUri(context, imgDoc, Uri.parse(tDoc.getFilelink()));
            imgDoc.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else {
            imgDoc.setImageResource(R.drawable.ic_add_documents_black_24dp);
            imgDoc.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        return vi;
    }
}
