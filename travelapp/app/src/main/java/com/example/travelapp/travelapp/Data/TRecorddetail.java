package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

/**
 * Created by danilofink on 21/11/17.
 */
public class TRecorddetail extends TBaseDbObject {

    // Columndefinitions
    public static final String CN_RECORD_ID         = "record_id";
    public static final String CD_RECORD_ID         = SCD_INT + " " + TravelAppDbHelper.References.COSTRECORD_ID;
    public static final String CN_MEMBER_ID         = "member_id";
    public static final String CD_MEMBER_ID         = SCD_INT + " " + TravelAppDbHelper.References.MEMBER_ID;
    public static final String CN_AMOUNT            = "amt";
    public static final String CD_AMOUNT            = SCD_FLOAT;

    // Membervariables
    private long recordId;
    private long memberId;
    private TMember member;
    private float amount;

    // Temporary variables
    private boolean checked;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.RECORDDETAILS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_RECORD_ID, CD_RECORD_ID),
                new Column(CN_MEMBER_ID, CD_MEMBER_ID),
                new Column(CN_AMOUNT, CD_AMOUNT),
        };
    }

    /** Empty Constructor. */
    public TRecorddetail() {
        setDefaultIdsAndReferences();
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setRecordId(INVALID_ID);
        setMemberId(INVALID_ID);
    }

    /** Gets the record id. */
    public long getRecordId() {
        return recordId;
    }

    /** Sets the record id. */
    public void setRecordId(long recordId) {
        this.recordId = recordId;
    }

    /** Gets the member id. */
    public long getMemberId() {
        return memberId;
    }

    /** Sets the member id. */
    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    /** Gets the associated member object from the database or the cache. */
    public TMember getMember(TravelAppDataSource dataSource) {
        long mId = getMemberId();
        if (member == null && mId != INVALID_ID) {
            member = new TMember();
            member.setId(mId);
            member = dataSource.selectObj(member);
        }
        return member;
    }

    /** Sets a new Member. But not directly in the database! */
    public void setMember(TMember val) {
        member = val;
        setMemberId(val.getId());
    }

    /** Gets the amount. */
    public float getAmount() {
        return amount;
    }

    /** Sets the amount. */
    public void setAmount(float amount) {
        this.amount = amount;
        setChecked(this.amount > 0);
    }

    public boolean isChecked() {
        return checked;
    }

    /** Sets checked */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_RECORD_ID         , getRecordId());
        cv.put(CN_MEMBER_ID         , getMemberId());
        cv.put(CN_AMOUNT            , getAmount());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TRecorddetail obj = new TRecorddetail();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setRecordId(DictionaryHelper.getInt(values, CN_RECORD_ID, INVALID_ID));
        obj.setMemberId(DictionaryHelper.getInt(values, CN_MEMBER_ID, INVALID_ID));
        obj.setAmount(DictionaryHelper.getFloat(values, CN_AMOUNT, 0f));
        return obj;
    }

}
