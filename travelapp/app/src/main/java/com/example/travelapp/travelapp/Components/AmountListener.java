package com.example.travelapp.travelapp.Components;

public interface AmountListener {
    void onAmountChanged(float newAmount);
}
