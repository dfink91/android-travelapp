package com.example.travelapp.travelapp.Components;

/**
 * Created by danilo on 16.01.18.
 */

public interface RefreshActivityListener {
    void onRefreshRequested();
}
