package com.example.travelapp.travelapp.Activities;

import android.os.Bundle;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.R;

/**
 * Created by danilofink on 03/01/18.
 */
public class BucketlistActivity extends MenuActivity {
    private static final String LOG_TAG = BucketlistActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_bucketlist);
    }
}
