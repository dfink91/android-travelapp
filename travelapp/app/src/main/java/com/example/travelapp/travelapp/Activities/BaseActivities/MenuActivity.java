package com.example.travelapp.travelapp.Activities.BaseActivities;

/*
* This is a simple and easy approach to reuse the same
* navigation drawer on your other activities. Just create
* a base layout that contains a DrawerLayout, the
* navigation drawer and a FrameLayout to hold your
* content view. All you have to do is to extend your
* activities from this class to set that navigation
* drawer. Happy hacking :)
* P.S: You don't need to declare this Activity in the
* AndroidManifest.xml. This is just a base class.
*/
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.menu.MenuItemImpl;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.travelapp.travelapp.Activities.CostManagement.CostOverviewActivity;
import com.example.travelapp.travelapp.Activities.Home.HomeActivity;
import com.example.travelapp.travelapp.Activities.PackingHelper.PackingHelperActivity;
import com.example.travelapp.travelapp.Activities.QuotesActivity;
import com.example.travelapp.travelapp.Activities.Wallet.WalletActivity;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.R;

public abstract class MenuActivity extends ScoccoActivity implements MenuItem.OnMenuItemClickListener {

    private static final String LOG_TAG = MenuActivity.class.getSimpleName();
    private static final String CURRENT_MENU_ITEM_ID = "CURRENT_MENU_ITEM_ID";

    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    TextView txtNavHeader;
    int currentMenuItemId;

    protected TravelAppDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_menu);// The base layout that contains your navigation drawer.
        view_stub = (FrameLayout) findViewById(R.id.view_stub);
        navigation_view = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 0, 0);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentMenuItemId = getIntent().getIntExtra(CURRENT_MENU_ITEM_ID, R.id.menu_home);
        drawerMenu = navigation_view.getMenu();
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }

        dataSource = new TravelAppDataSource(getApplicationContext());
        setupComponents();
    }

    private void setupComponents() {
        View headerLayout = navigation_view.getHeaderView(0);
        txtNavHeader = (TextView) headerLayout.findViewById(R.id.txt_menu_header_title);
        txtNavHeader.setOnClickListener(v -> {
            MenuActivity.this.openActivity(new Intent(MenuActivity.this, HomeActivity.class));
        });
        updateSelectedTripInSideNav(getCurrentTrip());
    }

    protected void updateSelectedTripInSideNav(TTrip trip) {
        if (txtNavHeader != null) {
            String name = "No trip selected!";
            boolean tripSelected = false;
            if (trip != null) {
                name = trip.getName();
                tripSelected = true;
            }
            txtNavHeader.setText(name);
            enableMenu(tripSelected);
        }
    }

    protected void enableMenu(boolean tripSelected) {
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setEnabled(tripSelected);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = null;
            if (inflater != null)
                stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item))
            return true;
        else
            onBackPressed();
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Intent intent = null;
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        if (item.getItemId() != this.currentMenuItemId) {
            switch (item.getItemId()) {
                case R.id.menu_home:
                    intent = new Intent(MenuActivity.this, HomeActivity.class);
                    break;
                case R.id.menu_cost_mgmt:
                    intent = new Intent(MenuActivity.this, CostOverviewActivity.class);
                    break;
                case R.id.menu_wallet:
                    intent = new Intent(MenuActivity.this, WalletActivity.class);
                    break;
                case R.id.menu_packinghelper:
                    intent = new Intent(MenuActivity.this, PackingHelperActivity.class);
                    break;
//              case R.id.menu_daily_journal:
//                  intent = new Intent(MenuActivity.this, JournalActivity.class);
//                  break;
                case R.id.menu_travel_quotes:
                    intent = new Intent(MenuActivity.this, QuotesActivity.class);
                    break;
//              case R.id.menu_group_chat:
//                  intent = new Intent(MenuActivity.this, ChatActivity.class);
//                  break;
//              case R.id.menu_bucketlist:
//                  intent = new Intent(MenuActivity.this, BucketlistActivity.class);
//                  break;
            }
            if (intent != null) {
                intent.putExtra(CURRENT_MENU_ITEM_ID, item.getItemId());
                openActivity(intent);
            }
        }
        return false;
    }

    private void openActivity(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    protected void setNavBarSideMenu() {
        // Show hamburger
        mDrawerToggle.setDrawerIndicatorEnabled(true);
    }

    protected void setNavBarNoSideMenu() {
        // Remove hamburger
        mDrawerToggle.setDrawerIndicatorEnabled(false);
    }

    protected void setNavBarDialog() {
        setNavBarNoSideMenu();

        // Show Close button
        final Drawable closeIcon = ContextCompat.getDrawable(this, R.drawable.ic_close_black_24dp);
        closeIcon.setColorFilter(getResources().getColor(R.color.colorTextLight), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(closeIcon);
    }
}