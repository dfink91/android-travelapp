package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

/**
 * Created by danilofink on 20/11/17.
 */

public class TItem extends TBaseDbObject {

    // Columndefinition
    public static final String CN_NAME          = "name";
    public static final String CD_NAME          = SCD_TEXT_NOT_NULL;

    // Membervariables
    private String name;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.ITEMS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_NAME, CD_NAME),
        };
    }

    /** Empty Constructor. */
    public TItem() {
        setDefaultIdsAndReferences();
    }

    /** Empty Constructor. */
    public TItem(String name) {
        this();
        setName(name);
    }

    /** Gets the name. */
    public String getName() {
        return name;
    }

    /** Sets the name.  */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_NAME          , getName());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TItem obj = new TItem();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setName(DictionaryHelper.getString(values, CN_NAME, ""));
        return obj;
    }
}
