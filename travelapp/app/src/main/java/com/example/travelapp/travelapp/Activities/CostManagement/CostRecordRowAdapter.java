package com.example.travelapp.travelapp.Activities.CostManagement;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.example.travelapp.travelapp.Components.AmountListener;
import com.example.travelapp.travelapp.Components.EditCurrency;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marius on 18.11.2017.
 */
public class CostRecordRowAdapter extends ArrayAdapter {
    Context context;
    AddCostActivity activity;
    List<TRecorddetail> recordDetails;
    TravelAppDataSource dataSource;

    public CostRecordRowAdapter(Context context, AddCostActivity activity, List<TRecorddetail> resource)
    {
        super(context, R.layout.row_costrecord_details,resource);
        this.context = context;
        this.activity = activity;
        recordDetails = new ArrayList<>();
        this.recordDetails = resource;
        dataSource = new TravelAppDataSource(context);
    }

    @Override
    public long getItemId(int position) {
        Object obj = super.getItem(position);
        if (obj instanceof TRecorddetail)
            return ((TRecorddetail)obj).getId();
        return super.getItemId(position);
    }

    /* Returns view item that is placed in the listview as a row_costrecord_details. */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.row_costrecord_details, parent, false);

        EditCurrency editTextAmount = convertView.findViewById(R.id.editText_amount);
        CheckBox checkBox = convertView.findViewById(R.id.checkbox_borrower);
        TRecorddetail rd = recordDetails.get(position);
        checkBox.setText(rd.getMember(dataSource).getName());
        editTextAmount.setTextAmount(rd.getAmount());
        editTextAmount.setEnabled(!activity.switch_equalShares.isChecked());

        if (rd.getAmount() > 0) {
            checkBox.setChecked(true);
            rd.setChecked(true);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkBox.isChecked()){
                    rd.setChecked(true);
                }
                else if (!checkBox.isChecked() && rd.isChecked()){
                    rd.setChecked(false);
                    editTextAmount.getText().clear();
                    rd.setAmount(0f);
                }
                if(activity.switch_equalShares.isChecked()) {
                    activity.splitMoney();
                }
                else {
                    activity.updateTotalAmount();
                }
            }
        });

        editTextAmount.addAmountListener(new AmountListener() {
            @Override
            public void onAmountChanged(float newAmount) {
                rd.setAmount(newAmount);
                checkBox.setChecked(rd.isChecked());
                if (!activity.switch_equalShares.isChecked())
                    activity.updateTotalAmount();
            }
        });
        return convertView;
    }

    public List<TRecorddetail> getRecorddetails(){
        return recordDetails;
    }

}
