package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.transition.TransitionValues;
import android.util.Log;

import com.example.travelapp.travelapp.Helper.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilofink on 12/11/17.
 */
public class TravelAppDataSource {

    private static final String LOG_TAG = TravelAppDataSource.class.getSimpleName();

    private SQLiteDatabase database;
    private TravelAppDbHelper dbHelper;

    /** Constructor: Initialises the dbHelper-Class. */
    public TravelAppDataSource(Context context) {
        Log.d(LOG_TAG, "Unsere DataSource erzeugt jetzt den dbHelper.");
        dbHelper = new TravelAppDbHelper(context);
        setApplicationDatasource(this);
    }

    private static TravelAppDataSource applicationDatasource;

    public static TravelAppDataSource getApplicationDatasource() {
        return applicationDatasource;
    }

    public static void setApplicationDatasource(TravelAppDataSource ds) {
        if (applicationDatasource == null)
            applicationDatasource = ds;
    }

    /**
     * Opens up a database connection.
     * Important don't forget to close it!
     */
    public void open() {
        Log.d(LOG_TAG, "Eine Referenz auf die Datenbank wird jetzt angefragt.");
        database = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "Datenbank-Referenz erhalten. Pfad zur Datenbank: " + database.getPath());
    }

    /** Closes the database connection. */
    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Datenbank mit Hilfe des DbHelpers geschlossen.");
    }

    /** Select a given object with only an id and returns the full one from the database. */
    public <T extends TBaseDbObject> T selectObj(T obj) {
        open();
        String selection = T.CN_ID + "= ?";
        Cursor cursor = database.query(obj.getTable(), obj.getColumns(),
                selection, new String[]{ "" + obj.getId() }, null, null, null);

        if (cursor.moveToFirst())
            obj = cursorToObj(cursor, obj);
        cursor.close();
        close();
        return obj;
    }

    /** Inserts the given object in the database. */
    public <T extends TBaseDbObject> T createObj(T obj) {
        String table = obj.getTable();
        open();
        long insertId = database.insert(table, null, obj.getContentValues(false));
        close();
        obj.setId(insertId);
        return obj;
    }

    /** Updates the given object in the database. */
    public <T extends TBaseDbObject> boolean updateObj(T obj) {
        open();
        boolean result = 1 == database.update( obj.getTable(), obj.getContentValues(false),
                T.CN_ID + "= ? ", new String[]{ "" + obj.getId() });
        close();
        return result;
    }

    /** Creates or updates the given object in the database. */
    public <T extends TBaseDbObject> T createOrUpdateObj(T obj) {
        if (obj.isNew())
            return createObj(obj);
        else
            return updateObj(obj) ? obj : null;
    }

    /** Delete the given object from the database. */
    public <T extends TBaseDbObject> boolean deleteObj(T obj) {
        open();
        boolean result = 1 == database.delete( obj.getTable(), T.CN_ID + "= ?", new String[]{ "" + obj.getId() });
        close();
        return result;
    }

    /** Transforms a database object into an IDatabaseObject. */
    private <T extends IDatabaseObject> T cursorToObj(Cursor cursor, T prototype) {
        ContentValues values = new ContentValues();
        for (String col : prototype.getColumns()) {
            addValueTo(values, cursor, col);
        }
        return (T)prototype.createObjWithContentVals(values);
    }

    /** Adds the database value to a given ContentValues object. */
    private void addValueTo(ContentValues values, Cursor cursor, String col) {
        int index = cursor.getColumnIndex(col);
        switch (cursor.getType(index)) {
            case Cursor.FIELD_TYPE_INTEGER:
                values.put(col, cursor.getInt(index));
                break;
            case Cursor.FIELD_TYPE_FLOAT:
                values.put(col, cursor.getFloat(index));
                break;
            case Cursor.FIELD_TYPE_STRING:
                values.put(col, cursor.getString(index));
                break;
            case Cursor.FIELD_TYPE_BLOB:
                values.put(col, cursor.getBlob(index));
                break;
            default:
                values.putNull(col);
        }
    }

    /** Gets all objects in the database given the type of parameter. */
    public <T extends IDatabaseObject> List<T> getAll(T prototype) {
        List<T> list = new ArrayList<>();
        open();
        Cursor cursor = database.query(prototype.getTable(),
                prototype.getColumns(), null, null, null, null, null);

        cursor.moveToFirst();
        T obj;

        while(!cursor.isAfterLast()) {
            obj = cursorToObj(cursor, prototype);
            list.add(obj);
            cursor.moveToNext();
        }

        cursor.close();
        close();
        return list;
    }

    public <T extends TBaseDbObject> ArrayList<T> getAllWhere(String field, String criteria, T prototype) {
        ArrayList<T> list = new ArrayList<>();
        if (!Util.isEmpty(field)) {
            String sql = "SELECT * FROM " +  prototype.getTable() + " "
                    + "WHERE " + field + "= ?";
            String[] params = new String[]{ criteria };
            ArrayList<ContentValues> rows = rawQuery(sql, params);
            if (rows != null) {
                for (ContentValues values : rows) {
                    list.add((T) prototype.createObjWithContentVals(values));
                }
            }
        }
        return list;
    }

    /** Gets all objects in the database given the sql query. */
    public ArrayList<ContentValues> rawQuery(String sql, String[] params) {
        ArrayList<ContentValues> list = new ArrayList<>();
        open();
        Cursor cursor = database.rawQuery(sql, params);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            ContentValues values = new ContentValues();
            for (String col : cursor.getColumnNames()) {
                addValueTo(values, cursor, col);
            }
            list.add(values);
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return list;
    }
}
