package com.example.travelapp.travelapp.Activities.CostManagement;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;

import com.example.travelapp.travelapp.Activities.BaseActivities.DialogActivity;
import com.example.travelapp.travelapp.Components.AmountListener;
import com.example.travelapp.travelapp.Components.EditCurrency;
import com.example.travelapp.travelapp.Components.EditDate;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilofink on 02/01/18.
 */
public class AddCostActivity extends DialogActivity {

    public final static String SELECTED_COST_RECORD = "SELECTED_COST_RECORD";

    //Components
    ListView listView;
    EditText editTextCostTitle;
    EditCurrency editTextTotalAmount;
    Spinner spinner;
    Switch switch_equalShares;
    EditDate editDate;

    //Class variables
    TCostrecord costrecord;
    ArrayList<TMember> members;
    ArrayList<TRecorddetail> allDetailsList;
    CostRecordRowAdapter costRecordRowAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_cost_management_add);

        costrecord = new TCostrecord();
        allDetailsList = new ArrayList<>();
        members = getCurrentTrip().getMembers(dataSource, true);

        setupComponents();

        long costrecordId = getIntent().getLongExtra(SELECTED_COST_RECORD, TCostrecord.INVALID_ID);
        if (costrecordId != TCostrecord.INVALID_ID) {
            costrecord = dataSource.selectObj(new TCostrecord(costrecordId));
            editTextCostTitle.setText(costrecord.getName());
            editDate.setDateSeconds(costrecord.getDate());
            float totalAmount = costrecord.getTotalAmount(dataSource, true);
            editTextTotalAmount.setTextAmount(totalAmount);
            Util.setSelectedMemberWithId(spinner, costrecord.getMemberId());
            populateRecorddetailsListView(costrecord.getRecordDetails(dataSource, true), members);
            setTitle(costrecord.getName());
        }
        else
            populateRecorddetailsListView(null, members);

        setupListeners();
    }

    private void populateRecorddetailsListView(ArrayList<TRecorddetail> rdList, ArrayList<TMember> mList) {
        for (TMember m : mList) {
            TRecorddetail rd = null;
            if (rdList != null) {
                for (TRecorddetail rdObj : rdList) {
                    if (rdObj.getMemberId() == m.getId()) {
                        rd = rdObj;
                        break;
                    }
                }
            }
            if (rd == null) {
                rd = new TRecorddetail();
                rd.setMemberId(m.getId());
                rd.setRecordId(costrecord.getId());
            }
            allDetailsList.add(rd);
        }
        refreshListViewSource();
    }

    private void refreshListViewSource() {
        costRecordRowAdapter = new CostRecordRowAdapter(this, this, allDetailsList);
        listView.setAdapter(costRecordRowAdapter);
    }

    @Override
    protected boolean saveObject() {
        boolean result = false;
        String name = editTextCostTitle.getText().toString().trim();
        if(!TextUtils.isEmpty(name)){
            if(costrecord == null)
                costrecord = new TCostrecord();
            costrecord.setName(name);
            costrecord.setDate(editDate.getDateSeconds());
            TMember m = (TMember)spinner.getSelectedItem();
            costrecord.setMemberId(m.getId());
            costrecord.setTripId(getCurrentTrip().getId());
            dataSource.createOrUpdateObj(costrecord);

            List<TRecorddetail> detailsList = costRecordRowAdapter.getRecorddetails();
            for (TRecorddetail rd: detailsList) {
                if(rd.getAmount() > 0){
                    rd.setRecordId(costrecord.getId());
                    dataSource.createOrUpdateObj(rd);
                }
                else if (!rd.isNew() && rd.getAmount() <= 0)
                    dataSource.deleteObj(rd);
            }
            result = true;
        }
        else {
            editTextCostTitle.setError("Please enter a title.");
        }
        return result;
    }

    public void setupComponents() {
        editTextCostTitle = (EditText) findViewById(R.id.editText_CostTitle);
        editDate = (EditDate) findViewById(R.id.editDate_costDate);
        spinner = (Spinner) findViewById(R.id.spinner_payed_by);
        editTextTotalAmount = (EditCurrency) findViewById(R.id.editText_totalAmount);
        switch_equalShares = (Switch) findViewById(R.id.switch_equalShares);
        listView = (ListView) findViewById(R.id.listView_borrowers);
        setSpinnerList();
    }

    public void setupListeners() {
        editTextTotalAmount.addAmountListener(new AmountListener() {
            @Override
            public void onAmountChanged(float newAmount) {
                if(switch_equalShares.isChecked())
                    splitMoney(newAmount);
            }
        });

        switch_equalShares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = editTextTotalAmount.getText().toString();
                if(switch_equalShares.isChecked()){
                    editTextTotalAmount.requestFocus();
                    splitMoney(editTextTotalAmount.getAmount());
                    editTextTotalAmount.setEnabled(true);
                }
                else if (!switch_equalShares.isChecked()){
                    editTextTotalAmount.setEnabled(false);
                }
                refreshListViewSource();
            }
        });
    }

    public void setSpinnerList(){
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, members);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);
    }

    public void splitMoney() {
        splitMoney(editTextTotalAmount.getAmount());
    }

    private void splitMoney(float amountToSplit){

        int checkedCounter = 0;

        int memberCount = Util.size(allDetailsList);
        for (TRecorddetail rd : allDetailsList) {
            if(rd.isChecked()) {
                checkedCounter++;
            }
        }
        float splitAmount = amountToSplit/memberCount;
        if (checkedCounter > 0)
            splitAmount = amountToSplit/checkedCounter;

        for (TRecorddetail rd : allDetailsList){
            if(checkedCounter <= 0) {
                rd.setAmount(splitAmount);
                rd.setChecked(true);
            }
            else if (rd.isChecked()) {
                rd.setAmount(splitAmount);
            }
        }
        refreshListViewSource();
    }

    public void updateTotalAmount() {
        float sum = 0;
        for (TRecorddetail rd : allDetailsList) {
            if(rd.isChecked()) {
                sum += rd.getAmount();
            }
        }
        editTextTotalAmount.setTextAmount(sum);
    }
}
