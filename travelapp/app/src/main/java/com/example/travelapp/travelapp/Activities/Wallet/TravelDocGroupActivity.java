package com.example.travelapp.travelapp.Activities.Wallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.widget.ListView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TTravelDoc;
import com.example.travelapp.travelapp.Data.TTravelDocGroup;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 06/01/18.
 */
public class TravelDocGroupActivity extends MenuActivity {

    private static final String LOG_TAG = TravelDocGroupActivity.class.getSimpleName();

    public static final String SELECTED_TRAVEL_DOC_GROUP = "SELECTED_TRAVEL_DOC_GROUP";

    private ListView listView;

    // Class variables
    TTravelDocGroup tdGroup;
    ArrayList<TTravelDoc> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_travel_doc_group);
        setNavBarNoSideMenu();


        tdGroup = new TTravelDocGroup();
        list = new ArrayList<>();

        long tdgId = getIntent().getLongExtra(SELECTED_TRAVEL_DOC_GROUP, TTravelDocGroup.INVALID_ID);
        if (tdgId != TTravelDocGroup.INVALID_ID) {
            tdGroup.setId(tdgId);
            tdGroup = dataSource.selectObj(tdGroup);
            setTitle(tdGroup.getTitle());
        }

        setupComponents();
        setupButtons();
    }

    @Override
    protected void onStart() {
        super.onStart();
        showAllListEntries();
    }

    private void setupComponents() {

    }

    private void showAllListEntries () {
        list = TTravelDoc.getAllForGroup(dataSource, tdGroup);
        listView = (ListView) findViewById(R.id.listview_docs);
        listView.setAdapter(new TravelDocRowAdapter(this, list));
        listView.setOnItemClickListener((parent, view, position, id) -> {
            startTravelDocActivity(id);
        });

    }

    private void startTravelDocActivity(long travelDocId) {
        Intent intent = new Intent(TravelDocGroupActivity.this, TravelDocActivity.class);
        intent.putExtra(SELECTED_TRAVEL_DOC_GROUP, tdGroup.getId());
        intent.putExtra(TravelDocActivity.SELECTED_TRAVEL_DOC, travelDocId);
        startActivity(intent);
    }

    private void setupButtons() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(v -> startTravelDocActivity(TTravelDoc.INVALID_ID));
    }
}
