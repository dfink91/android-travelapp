package com.example.travelapp.travelapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;

/**
 * Created by danilofink on 05/01/18.
 */
public class ScoccoApplication extends Application {

    private static final String SCOCCO_SETTINGS = "SCOCCO_SETTINGS";
    private static final String PREF_TRIP_ID    = "PREF_TRIP_ID";
    private TTrip currentTrip;
    private boolean fetchedFromSettings;

    public void setCurrentTrip(TTrip trip) {
        if (trip != null && !trip.equals(currentTrip)) {
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                    SCOCCO_SETTINGS, Context.MODE_PRIVATE
            );
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong(PREF_TRIP_ID, trip.getId());
            editor.commit();
            currentTrip = trip;
        }
    }

    public TTrip getCurrentTrip() {
        if (!fetchedFromSettings && currentTrip == null) {
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                SCOCCO_SETTINGS, Context.MODE_PRIVATE
            );
            long id = sharedPref.getLong(PREF_TRIP_ID, -1);
            if (id >= 0) {
                TTrip trip = new TTrip(id, null);
                this.currentTrip = new TravelAppDataSource(getApplicationContext()).selectObj(trip);
            }
            fetchedFromSettings = true;
        }
        return currentTrip;
    }
}
