package com.example.travelapp.travelapp.Activities.PackingHelper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.travelapp.travelapp.Components.RefreshActivityListener;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TItem;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TPackinglist;
import com.example.travelapp.travelapp.Data.TPackinglistItem;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.Data.TTravelDoc;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.Helper.UtilExternal;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 17/01/18.
 */
public class PackingItemRowAdapter extends ArrayAdapter {

    private Context context;
    private TravelAppDataSource dataSource;
    private ArrayList<TPackinglistItem> data;
    private static LayoutInflater inflater = null;

    public PackingItemRowAdapter(Context context, ArrayList<TPackinglistItem> data) {
        super(context, R.layout.row_packing_item, data);
        this.context = context;
        dataSource = new TravelAppDataSource(context);
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.row_packing_item, null);
        final CheckBox chkItem = vi.findViewById(R.id.checkbox_item);
        final TPackinglistItem tPItem = data.get(position);
        final TItem tItem = data.get(position).getItem(dataSource);
        chkItem.setChecked(tPItem.isChecked());
        chkItem.setText(tItem.getName());
        chkItem.setOnClickListener(v -> {
            tPItem.setChecked(chkItem.isChecked());
            dataSource.updateObj(tPItem);
        });
        ImageButton btnDelete = vi.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(v -> {
            new AlertDialog.Builder(getContext())
                .setTitle("Delete")
                .setMessage("Do you want to delete?")
                .setIcon(R.drawable.ic_close_black_24dp)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        dataSource.deleteObj(data.get(position));
                        data.remove(position);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
        });
        return vi;
    }
}
