package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.travelapp.travelapp.Helper.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilofink on 20/11/17.
 */
public abstract class TBaseDbObject implements IDatabaseObject {

    //Columndefinitions
    // Every field has to specify its column name (CN_*)
    // and a column definition (CD_*)
    // Basic autoincrement ID field
    /** CN_ID is the Column Name of the basic autoincremented id field for every table. */
    public static final String CN_ID = "_id";
    /** CD_ID is the Column Definition of the basic autoincremented id field for every table. */
    public static final String CD_ID = SCD_PRIMARY_KEY_AUTOINC;

    // Membervariables
    private Long id;

    /** Gets the id of a database object. */
    public long getId() { return id; }

    /** Gets the id of a database object as a String. */
    public String getIdStr() { return "" + id; }

    /** Sets the id of a database object. */
    public void setId(long id) { this.id = id; }

    /** Saves an object in the Database. */
    public void saveInDB(TravelAppDataSource dataSource) {
        dataSource.createOrUpdateObj(this);
    }

    /** Sets the id and the reference ids to default values. */
    protected void setDefaultIdsAndReferences() {
        setId(INVALID_ID);
    }

    /** Checks if an object is not yet inserted in the Database. */
    public boolean isNew() {
        return getId() == INVALID_ID;
    }

    /** Gets the display name of an object. */
    public String getDisplayName() {
        return getIdStr();
    }

    /** Sets the display name of an object. */
    public void setDisplayName(String val) {
    }
}
