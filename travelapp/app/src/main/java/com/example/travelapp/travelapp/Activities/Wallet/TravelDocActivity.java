package com.example.travelapp.travelapp.Activities.Wallet;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.travelapp.travelapp.Activities.BaseActivities.DialogActivity;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TTravelDoc;
import com.example.travelapp.travelapp.Data.TTravelDocGroup;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.Helper.UtilExternal;
import com.example.travelapp.travelapp.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by danilofink on 07/01/18.
 */
public class TravelDocActivity extends DialogActivity {

    private static final String LOG_TAG = TravelDocActivity.class.getSimpleName();

    public static final String SELECTED_TRAVEL_DOC = "SELECTED_TRAVEL_DOC";

    // Components
    EditText txtTitle;
    Spinner spinnerMember;
    Button btnAddEditDoc;
    ImageView imgView;

    // Class variables
    long travelDocGroupId;
    TTravelDoc travelDoc;
    File photoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_travel_doc);

        setTitle("New Document...");
        setupComponents();
        setupButtons();

        travelDoc = new TTravelDoc();
        travelDocGroupId = getIntent().getLongExtra(
                TravelDocGroupActivity.SELECTED_TRAVEL_DOC_GROUP, TTravelDocGroup.INVALID_ID);
        long travelDocId = getIntent().getLongExtra(SELECTED_TRAVEL_DOC, TTravelDoc.INVALID_ID);
        if (travelDocId != TTravelDoc.INVALID_ID) {
            travelDoc.setId(travelDocId);
            travelDoc = dataSource.selectObj(travelDoc);
            TMember m = travelDoc.getMember(dataSource);
            setTitle(travelDoc.getTitle() + " - " + (m != null ? m.getName() : "no owner"));
            txtTitle.setText(travelDoc.getTitle());
            Util.setSelectedMemberWithId(spinnerMember, travelDoc.getMemberId());
            if (!Util.isEmpty(travelDoc.getFilelink()))
                UtilExternal.setImageViewWithUri(this, imgView, Uri.parse(travelDoc.getFilelink()));
            else
                btnAddEditDoc.setText(R.string.button_edit);
        }
    }

    private void setupComponents() {
        txtTitle = (EditText) findViewById(R.id.txt_title);
        spinnerMember = (Spinner) findViewById(R.id.spinner_member);
        btnAddEditDoc = (Button) findViewById(R.id.btn_add_edit);
        imgView = (ImageView) findViewById(R.id.imgView_document);
        ArrayList<TMember> members = (ArrayList<TMember>) getCurrentTrip().getMembers(dataSource, false).clone();
        members.add(0, new TMember("nobody"));
        ArrayAdapter <TMember> arrayAdapter =
                new ArrayAdapter<TMember>(this, android.R.layout.simple_spinner_item, members);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMember.setAdapter(arrayAdapter);
    }

    private void setupButtons() {
        btnAddEditDoc.setOnClickListener(v -> openPhotoOrGallery());
    }


    private String userChoosenTask;

    private void openPhotoOrGallery () {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(TravelDocActivity.this);
        builder.setTitle("Add Document!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = UtilExternal.checkPermission(TravelDocActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask="Take Photo";
                    if(result)
                        photoFile = UtilExternal.takePictureIntent(TravelDocActivity.this);

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        UtilExternal.openGalleryIntent(TravelDocActivity.this);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case UtilExternal.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        photoFile = UtilExternal.takePictureIntent(this);
                    else if(userChoosenTask.equals("Choose from Library"))
                        UtilExternal.openGalleryIntent(this);
                } else {
                    //code for deny
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri uri = null;
            if (requestCode == UtilExternal.REQUEST_IMAGE_CAPTURE)
                uri = UtilExternal.getUriFromCaptureImageResult(photoFile);
            if (requestCode == UtilExternal.REQUEST_IMAGE_GALLERY)
                uri = UtilExternal.getUriFromSelectFromGalleryResult(data);
            if (requestCode == UtilExternal.REQUEST_IMAGE_GALLERY_KITKAT)
                uri = UtilExternal.getUriFromSelectFromGalleryResultKitKat(this, data);
            if (uri != null) {
                travelDoc.setFilelink(uri.toString());
                UtilExternal.setImageViewWithUri(this, imgView, uri);
            }
        }
    }

    @Override
    protected boolean saveObject() {
        boolean result = false;
        String val = txtTitle.getText().toString();
        if(TextUtils.isEmpty(val))
            txtTitle.setError(getString(R.string.editText_error_msg_empty));
        else if (travelDocGroupId == TTravelDocGroup.INVALID_ID)
            Toast.makeText(this, "Not able to save object.", Toast.LENGTH_SHORT).show();
        else {
            if (travelDoc == null)
                travelDoc = new TTravelDoc();
            travelDoc.setTravelDocGroupId(travelDocGroupId);
            travelDoc.setTitle(val);
            TMember m = (TMember)spinnerMember.getSelectedItem();
            travelDoc.setMemberId(m.getId());
            //trip.setFrom(txtFrom.getText().toString());
            //trip.setTo(txtTo.getText().toString());
            dataSource.createOrUpdateObj(travelDoc);
            result = true;
        }
        return result;
    }
}
