package com.example.travelapp.travelapp.Data;

import android.os.AsyncTask;
import android.util.Log;

import com.example.travelapp.travelapp.Activities.QuotesActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class QuotesRequest extends AsyncTask<Void, Void, JSONObject> {

    private QuotesActivity activity;

    public QuotesRequest (QuotesActivity activity) {
        this.activity = activity;
    }

    @Override
    protected JSONObject doInBackground(Void... params)
    {
        String str="https://talaikis.com/api/quotes/random/";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        try
        {
            URL url = new URL(str);
            urlConn = url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuffer.append(line);
            }
            JSONObject jsonObject = new JSONObject(stringBuffer.toString());
            return jsonObject;
        }
        catch(Exception ex)
        {
            Log.e("App", "QuotesRequest", ex);
            return null;
        }
        finally
        {
            if(bufferedReader != null)
            {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPostExecute(JSONObject response)
    {
        activity.setQuote(response);
    }
}



