package com.example.travelapp.travelapp.Activities.Wallet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TTravelDocGroup;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 03/01/18.
 */
public class WalletActivity extends MenuActivity {

    private static final String LOG_TAG = WalletActivity.class.getSimpleName();

    private ListView listView;
    private EditText txtAddGroup;


    ArrayList<TTravelDocGroup> tdGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_wallet);

        setupComponents();
        setupButtons();
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshTDGroupsList();
    }

    private void setupComponents() {
        txtAddGroup = (EditText) findViewById(R.id.txt_wallet_add_group);
    }

    private void refreshTDGroupsList() {
        tdGroups = TTravelDocGroup.getAllForTrip(dataSource, getCurrentTrip());
        listView = (ListView) findViewById(R.id.listview_wallet);
        listView.setAdapter(new TravelDocGroupRowAdapter(this, tdGroups));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(WalletActivity.this, TravelDocGroupActivity.class);
                intent.putExtra(TravelDocGroupActivity.SELECTED_TRAVEL_DOC_GROUP, id);
                startActivity(intent);
            }
        });
    }

    public void onBtnAddTripMember(View v) {
        String val = txtAddGroup.getText().toString();
        if(!TextUtils.isEmpty(val)) {
            TTravelDocGroup obj = new TTravelDocGroup(getCurrentTrip(), val);
            obj.saveInDB(dataSource);
            txtAddGroup.setText(null);
            refreshTDGroupsList();
        }
        else
            txtAddGroup.setError(getString(R.string.editText_error_msg_empty));
    }

    private void setupButtons() {
        Button btn = (Button) findViewById(R.id.button_add);
        btn.setOnClickListener(this::onBtnAddTripMember);
    }
}
