package com.example.travelapp.travelapp.Components;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.travelapp.travelapp.Data.TBaseDbObject;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TItem;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilofink on 25/01/18.
 */
public class SimpleObjectRowAdapter <T extends TBaseDbObject> extends ArrayAdapter {

    List<T> data;
    Context context;
    TravelAppDataSource dataSource;
    TextView tv_title;
    ImageButton btn_delete;
    ImageButton btn_edit;
    private static LayoutInflater inflater = null;

    public SimpleObjectRowAdapter(@NonNull Context context, List<T> resource) {
        super(context, R.layout.row_simple_object, resource);
        this.context = context;
        this.data = resource;
        this.dataSource = new TravelAppDataSource(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.row_simple_object, null);
        tv_title = vi.findViewById(R.id.textView_title);
        TBaseDbObject obj = data.get(position);
        tv_title.setText(obj.getDisplayName());
        ImageButton btnDelete = vi.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(v -> {
            new AlertDialog.Builder(getContext())
                    .setTitle("Delete")
                    .setMessage("Do you want to delete?")
                    .setIcon(R.drawable.ic_close_black_24dp)
                    .setPositiveButton("Delete", (dialog, whichButton) -> {
                        dialog.dismiss();
                        boolean success = dataSource.deleteObj(data.get(position));
                        if (success) {
                            data.remove(position);
                            notifyDataSetChanged();
                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle("Ooops")
                                    .setMessage("Something went wrong!")
                                    .show();
                        }
                    })
                    .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                    .show();
        });
        final EditText edit_changed = new EditText(context);
        edit_changed.setText(obj.getDisplayName());
        final AlertDialog changeDialog = new AlertDialog.Builder(getContext())
                .setTitle("Change it!")
                .setIcon(R.drawable.ic_edit_black_24dp)
                .setView(edit_changed)
                .setPositiveButton("ok", (dialog, whichButton) -> {
                    dialog.dismiss();
                    TBaseDbObject tempObj = data.get(position);
                    tempObj.setDisplayName(edit_changed.getText().toString());
                    dataSource.createOrUpdateObj(tempObj);
                    notifyDataSetChanged();
                })
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                .create();
        vi.setOnClickListener(v -> changeDialog.show());
        vi.findViewById(R.id.btn_edit).setOnClickListener(v -> changeDialog.show());
        return vi;
    }
}
