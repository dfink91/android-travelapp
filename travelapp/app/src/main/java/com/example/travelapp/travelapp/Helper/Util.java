package com.example.travelapp.travelapp.Helper;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Spinner;

import com.example.travelapp.travelapp.Data.TMember;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

/**
 * Created by danilofink on 05/01/18.
 */
public abstract class Util {

    /** Returns the String or a default value, if the String is empty. */
    public static String stringOrDefault(String str, String defaultStr) {
        if (!isEmpty(str))
            return str;
        return defaultStr;
    }

    /** Checks if a String is null or empty. */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /** Returns the size of a collection. Null-tolerant! */
    public static int size(Collection collection) {
        if (collection != null)
            return collection.size();
        return 0;
    }

    /** Converts a long millisecond timestamp into a int UNIX timestamp. */
    public static int getSeconds(long time) {
        return (int)(time/1000);
    }

    /** Gets a nice looking Date from a UNIX timestamp (seconds) */
    public static String getFormattedDate_dd_MMM_yy(int time, Context context) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd. MMM. yy", getCurrentLocale(context));
        return sdf.format(new Date(time * 1000L));
    }

    public static Locale getCurrentLocale(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    public static void setSelectedMemberWithId(Spinner spinner, long id) {
        int cnt = spinner.getAdapter().getCount();
        for (int i = 0; i < cnt; i++) {
            TMember m = (TMember) spinner.getItemAtPosition(i);
            if (m.getId() == id) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public static String convertToCurrency(float val, String delimiter, String currency) {
        int cleanVal = (int)(val * 100);
        String valStr = cleanVal + "";
        String str = cleanVal/100 + delimiter + (cleanVal != 0 ? valStr.substring(valStr.length() - 2) : "00");
        if(!isEmpty(currency)){
            str += " " + currency;
        }
        return str;
    }
}
