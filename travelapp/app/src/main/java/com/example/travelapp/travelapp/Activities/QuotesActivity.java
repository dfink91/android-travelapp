package com.example.travelapp.travelapp.Activities;

import android.os.Bundle;
import android.widget.TextView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.QuotesRequest;
import com.example.travelapp.travelapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by danilofink on 03/01/18.
 */
public class QuotesActivity extends MenuActivity{

    private static final String LOG_TAG = QuotesActivity.class.getSimpleName();

    //Components
    TextView textViewQuotes;
    TextView textViewAuthor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_quotes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupComponent();
        setQuote(null);
        new QuotesRequest(this).execute();
    }

    private void setupComponent() {
        textViewQuotes = (TextView) findViewById(R.id.textView_quotes);
        textViewAuthor = (TextView) findViewById(R.id.textView_quotesAuthor);
    }

    public void setQuote(JSONObject obj) {
        String quote = "Quote is loading...";
        String author = "";
        if (obj != null){
            try {
                quote = obj.getString("quote");
                //quote = "Ich habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagtIch habe gesagt!";
                quote = "\"" + quote + "\"";
                author = obj.getString("author");
                author = "- " + author + " -" ;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        textViewQuotes.setText(quote);
        textViewAuthor.setText(author);
    }
}
