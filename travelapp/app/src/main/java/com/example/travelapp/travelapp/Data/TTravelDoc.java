package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

import java.util.ArrayList;

/**
 * Created by danilofink on 21/11/17.
 */
public class TTravelDoc extends TBaseDbObject {

    // Columndefinition
    public static final String CN_TRAVELDOC_GROUP_ID    = "tdg_id";
    public static final String CD_TRAVELDOC_GROUP_ID    = SCD_INT + " " + TravelAppDbHelper.References.TRAVELDOC_GROUP_ID;
    public static final String CN_MEMBER_ID             = "member_id";
    public static final String CD_MEMBER_ID             = SCD_INT + " " + TravelAppDbHelper.References.MEMBER_ID;
    public static final String CN_TITLE                 = "title";
    public static final String CD_TITLE                 = SCD_TEXT_NOT_NULL;
    public static final String CN_FILELINK              = "filelink";
    public static final String CD_FILELINK              = SCD_TEXT;
    public static final String CN_FROM                  = "fromDate";
    public static final String CD_FROM                  = SCD_INT;
    public static final String CN_TO                    = "toDate";
    public static final String CD_TO                    = SCD_INT;

    // Membervariables
    private long travelDocGroupId;
    private long memberId;
    private TMember member;
    private String title;
    private String filelink;
    private int from;
    private int to;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.TRAVELDOCS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_TRAVELDOC_GROUP_ID, CD_TRAVELDOC_GROUP_ID),
                new Column(CN_MEMBER_ID, CD_MEMBER_ID),
                new Column(CN_TITLE, CD_TITLE),
                new Column(CN_FILELINK, CD_FILELINK),
                new Column(CN_FROM, CD_FROM),
                new Column(CN_TO, CD_TO),
        };
    }

    /** Empty Constructor. */
    public TTravelDoc() {
        setDefaultIdsAndReferences();
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setTravelDocGroupId(INVALID_ID);
        setMemberId(INVALID_ID);
    }

    /** Gets the traveldoc_group id. */
    public long getTravelDocGroupId() {
        return travelDocGroupId;
    }

    /** Sets the traveldoc_group id. */
    public void setTravelDocGroupId(long val) {
        this.travelDocGroupId = val;
    }

    /** Gets the member id. */
    public long getMemberId() {
        return memberId;
    }

    /** Sets the member id. */
    public void setMemberId(long val) {
        this.memberId = val;
    }

    /** Gets the associated member object from the database or the cache. */
    public TMember getMember(TravelAppDataSource dataSource) {
        long mId = getMemberId();
        if (member == null && mId != INVALID_ID) {
            member = new TMember();
            member.setId(mId);
            member = dataSource.selectObj(member);
        }
        return member;
    }

    /** Sets a new Member. But not directly in the database! */
    public void setMember(TMember val) {
        member = val;
        setMemberId(val.getId());
    }

    /** Gets the title. */
    public String getTitle() { return title; }

    /** Sets the title. */
    public void setTitle(String val) { this.title = val; }

    /** Gets the filelink. */
    public String getFilelink() { return filelink; }

    /** Sets the filelink. */
    public void setFilelink(String val) { this.filelink = val; }

    /** Gets the start of a trip in seconds. */
    public int getFrom() {
        return from;
    }

    /** Sets the start of a trip in seconds. */
    public void setFrom(int from) {
        this.from = from;
    }

    /** Gets the end of a trip in seconds. */
    public int getTo() {
        return to;
    }

    /** Sets the end of a trip in seconds. */
    public void setTo(int to) {
        this.to = to;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_TRAVELDOC_GROUP_ID, getTravelDocGroupId());
        cv.put(CN_MEMBER_ID         , getMemberId());
        cv.put(CN_TITLE             , getTitle());
        cv.put(CN_FILELINK          , getFilelink());
        cv.put(CN_FROM              , getFrom());
        cv.put(CN_TO                , getTo());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TTravelDoc obj = new TTravelDoc();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setTravelDocGroupId(DictionaryHelper.getInt(values, CN_TRAVELDOC_GROUP_ID, INVALID_ID));
        obj.setMemberId(DictionaryHelper.getInt(values, CN_MEMBER_ID, INVALID_ID));
        obj.setTitle(DictionaryHelper.getString(values, CN_TITLE, ""));
        obj.setFilelink(DictionaryHelper.getString(values, CN_FILELINK, ""));
        obj.setFrom(DictionaryHelper.getInt(values, CN_FROM, 0));
        obj.setTo(DictionaryHelper.getInt(values, CN_TO, 0));
        return obj;
    }

    public static ArrayList<TTravelDoc> getAllForGroup(TravelAppDataSource dataSource, TTravelDocGroup holder) {
        ArrayList<TTravelDoc> list = new ArrayList<>();
        if (dataSource != null && holder != null) {
            TTravelDoc obj = new TTravelDoc();
            list = dataSource.getAllWhere(TTravelDoc.CN_TRAVELDOC_GROUP_ID, holder.getIdStr(), obj);
        }
        return list;
    }

}
