package com.example.travelapp.travelapp.Activities.Home;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.travelapp.travelapp.Activities.BaseActivities.DialogActivity;
import com.example.travelapp.travelapp.Components.EditDate;
import com.example.travelapp.travelapp.Components.SimpleObjectRowAdapter;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Components.NonScrollListView;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 03/01/18.
 */
public class TripActivity extends DialogActivity {

    private static final String LOG_TAG = TripActivity.class.getSimpleName();

    public final static String SELECTED_TRIP = "SELECTED_TRIP";

    // Components
    EditText txtName;
    EditText txtDestination;
    EditDate txtFrom;
    EditDate txtTo;
    EditText txtAddMember;

    // Class variables
    TTrip trip;
    ArrayList<TMember> members;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_trip);

        setupComponents();
        setupButtons();

        trip = new TTrip();
        members = new ArrayList<>();

        long tripId = getIntent().getLongExtra(SELECTED_TRIP, TTrip.INVALID_ID);
        if (tripId != TTrip.INVALID_ID) {
            trip = dataSource.selectObj(new TTrip(tripId, null));
            txtName.setText(trip.getName());
            txtDestination.setText(trip.getDestination());
            txtFrom.setDateSeconds(trip.getFrom());
            txtTo.setDateSeconds(trip.getTo());
            members = trip.getMembers(dataSource, false);
        }

        refreshMembersList();
    }

    private void setupComponents() {
        txtName = (EditText) findViewById(R.id.txtNewTripName);
        txtDestination = (EditText) findViewById(R.id.txtNewTripDestination);
        txtFrom = (EditDate) findViewById(R.id.txtNewTripFrom);
        txtTo = (EditDate) findViewById(R.id.txtNewTripTo);
        txtAddMember = (EditText) findViewById(R.id.txtNewTripAddMember);
    }


    private void refreshMembersList () {
        SimpleObjectRowAdapter adapter = new SimpleObjectRowAdapter(this, members);
        NonScrollListView listView = (NonScrollListView) findViewById(R.id.listview_members);
        listView.setAdapter(adapter);
    }

    @Override
    protected boolean saveObject() {
        boolean result = true;
        String name = txtName.getText().toString();
        if(TextUtils.isEmpty(name)) {
            txtName.setError(getString(R.string.editText_error_msg_empty));
            result = false;
        }
        if(txtTo.getDateSeconds() != 0 && txtFrom.getDateSeconds() > txtTo.getDateSeconds()) {
            txtTo.setDate(null);
            txtTo.setText("Choose a later date!");
            txtTo.setError("This date must be after the start date.");
            result = false;
        }
        if(Util.size(members) <= 0) {
            txtAddMember.setError("Add at least one member.");
            result = false;
        }

        if (result) {
            if (trip == null)
                trip = new TTrip();
            trip.setName(name);
            trip.setDestination(txtDestination.getText().toString());
            trip.setFrom(txtFrom.getDateSeconds());
            trip.setTo(txtTo.getDateSeconds());
            dataSource.createOrUpdateObj(trip);
            for (TMember m : members)
                trip.addMemberIfNeeded(dataSource, m);
        }

        Intent data = new Intent();
        data.putExtra(TripActivity.SELECTED_TRIP, trip.getId());
        setResult(RESULT_OK, data);

        return result;
    }

    public void onBtnAddTripMember(View v) {
        String name = txtAddMember.getText().toString();
        if(!TextUtils.isEmpty(name)) {
            TMember m = new TMember();
            m.setName(name);
            members.add(m);
            refreshMembersList();
            txtAddMember.setText(null);
        }
        else
            txtName.setError(getString(R.string.editText_error_msg_empty));
    }

    private void setupButtons() {
        Button btn = (Button) findViewById(R.id.button_add);
        btn.setOnClickListener(this::onBtnAddTripMember);
    }

}
