package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

/**
 * Created by danilofink on 21/11/17.
 */
public class TTripMember extends TBaseDbObject {

    // Columndefinition
    public static final String CN_TRIP_ID       = "trip_id";
    public static final String CD_TRIP_ID       = SCD_INT + " " + TravelAppDbHelper.References.TRIP_ID;
    public static final String CN_MEMBER_ID     = "member_id";
    public static final String CD_MEMBER_ID     = SCD_INT + " " + TravelAppDbHelper.References.MEMBER_ID;

    // Membervariables
    private long tripId;
    private long memberId;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.TRIP_MEMBER;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_TRIP_ID, CD_TRIP_ID),
                new Column(CN_MEMBER_ID, CD_MEMBER_ID)
        };
    }

    /** Empty Constructor. */
    public TTripMember() {
        setDefaultIdsAndReferences();
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setTripId(INVALID_ID);
        setMemberId(INVALID_ID);
    }

    /** Gets the trip id. */
    public long getTripId() {
        return tripId;
    }

    /** Sets the trip id. */
    public void setTripId(long val) {
        this.tripId = val;
    }

    /** Gets the member id. */
    public long getMemberId() {
        return memberId;
    }

    /** Sets the member id. */
    public void setMemberId(long val) {
        this.memberId = val;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_TRIP_ID, getTripId());
        cv.put(CN_MEMBER_ID, getMemberId());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TTripMember obj = new TTripMember();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setTripId(DictionaryHelper.getInt(values, CN_TRIP_ID, INVALID_ID));
        obj.setMemberId(DictionaryHelper.getInt(values, CN_MEMBER_ID, INVALID_ID));
        return obj;
    }
}
