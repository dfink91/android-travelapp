package com.example.travelapp.travelapp.Activities.Home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.R;
import com.example.travelapp.travelapp.ScoccoApplication;

import java.util.List;

public class HomeActivity extends MenuActivity {
    private static final String LOG_TAG = HomeActivity.class.getSimpleName();

    private ListView listTrips;
    private List<TTrip> tripList;
    private TTrip currentTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_home);
        setupButtons();
        setupComponents();

        tripList = dataSource.getAll(new TTrip());

        currentTrip = getCurrentTrip();
    }

    private void setupComponents() {
    }

    private void setSelectedTripAndUpdateComponents(TTrip trip) {
        currentTrip = trip;
        ((ScoccoApplication)getApplication()).setCurrentTrip(currentTrip);
        updateSelectedTripInSideNav(currentTrip);
        TextView txtName = (TextView) findViewById(R.id.txt_selected_trip_name);
        LinearLayout llDetails = (LinearLayout) findViewById(R.id.ll_selected_trip_details);
        if (currentTrip != null) {
            txtName.setText(currentTrip.getName());
            llDetails.setVisibility(View.VISIBLE);
            TextView txtDestination = (TextView) findViewById(R.id.txt_selected_trip_destination);
            txtDestination.setText(Util.stringOrDefault(currentTrip.getDestination(), "Destination not known!"));

            TextView txtViewFrom = (TextView) findViewById(R.id.txtView_selected_trip_from);
            TextView txtFrom = (TextView) findViewById(R.id.txt_selected_trip_from);
            TextView txtViewTo = (TextView) findViewById(R.id.txtView_selected_trip_to);
            TextView txtTo = (TextView) findViewById(R.id.txt_selected_trip_to);
            int timeVisibility = View.GONE;
            if ((long) currentTrip.getFrom() * currentTrip.getTo() > 0) {
                txtFrom.setText(Util.getFormattedDate_dd_MMM_yy(currentTrip.getFrom(), this));
                txtTo.setText(Util.getFormattedDate_dd_MMM_yy(currentTrip.getTo(), this));
                timeVisibility = View.VISIBLE;
            }
            txtViewFrom.setVisibility(timeVisibility);
            txtFrom.setVisibility(timeVisibility);
            txtViewTo.setVisibility(timeVisibility);
            txtTo.setVisibility(timeVisibility);

            TextView txtGroupSize = (TextView) findViewById(R.id.txt_selected_trip_groupsize);
            txtGroupSize.setText("" + currentTrip.getMembers(dataSource, false).size());
        }
        else {
            txtName.setText("Please choose a trip...");
            txtName.setTextSize(22);
            llDetails.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (currentTrip == null && tripList.size() >= 1)
            setSelectedTripAndUpdateComponents(tripList.get(0));
        else
            setSelectedTripAndUpdateComponents(currentTrip);
    }

    public void onBtnEditSelectedTripClicked(View v) {
        if (currentTrip != null) {
            Intent intent = new Intent(HomeActivity.this, TripActivity.class);
            intent.putExtra(TripActivity.SELECTED_TRIP, currentTrip.getId());
            startActivityForResult(intent, 0);
        }
    }

    public void onBtnSelectTrip(View v) {
        startActivityForResult(new Intent(HomeActivity.this, TripsActivity.class), 0);
    }

    private void setupButtons() {
        Button btn = (Button) findViewById(R.id.btn_edit_selected_trip);
        btn.setOnClickListener(this::onBtnEditSelectedTripClicked);

        btn = (Button) findViewById(R.id.btn_select_other_trip);
        btn.setOnClickListener(this::onBtnSelectTrip);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            TTrip trip = new TTrip();
            trip.setId(data.getLongExtra(TripActivity.SELECTED_TRIP, TTrip.INVALID_ID));
            if (trip.getId() != TTrip.INVALID_ID)
                currentTrip = dataSource.selectObj(trip);
        }
    }
}
