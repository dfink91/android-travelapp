package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

import java.util.ArrayList;

/**
 * Created by danilofink on 12/11/17.
 */
public class TTrip extends TBaseDbObject {

    // Columndefinitions
    public static final String CN_PACKINGLIST_ID= "packinglist_id";
    public static final String CD_PACKINGLIST_ID= SCD_INT + " " + TravelAppDbHelper.References.PACKINGLIST_ID;
    public static final String CN_NAME          = "name";
    public static final String CD_NAME          = SCD_TEXT_NOT_NULL;
    public static final String CN_DESTINATION   = "destination";
    public static final String CD_DESTINATION   = SCD_TEXT;
    public static final String CN_FROM          = "fromDate";
    public static final String CD_FROM          = SCD_INT;
    public static final String CN_TO            = "toDate";
    public static final String CD_TO            = SCD_INT;

    // Membervariables
    private long packinglistId;
    private String name;
    private String destination;
    private int from;
    private int to;

    // Temporary variables
    private ArrayList<TMember> members;
    private ArrayList<TCostrecord> costrecords;


    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.TRIPS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_PACKINGLIST_ID, CD_PACKINGLIST_ID),
                new Column(CN_NAME, CD_NAME),
                new Column(CN_DESTINATION, CD_DESTINATION),
                new Column(CN_FROM, CD_FROM),
                new Column(CN_TO, CD_TO),
        };
    }

    /** Empty Constructor. */
    public TTrip() {
        setDefaultIdsAndReferences();
    }

    /** Constructor 1. */
    public TTrip(long id, String name) {
        setDefaultIdsAndReferences();
        setId(id);
        setName(name);
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
    }

    /** Gets the id of the assigned packinglist. */
    public long getPackinglistId() {
        return packinglistId;
    }

    /** Sets the id of the assigned packinglist. */
    public void setPackinglistId(long packinglistId) {
        this.packinglistId = packinglistId;
    }

    /** Gets the name of a trip. */
    public String getName() {
        return name;
    }

    /** Sets the name of a trip.  */
    public void setName(String name) {
        this.name = name;
    }

    /** Gets the destination of a trip. */
    public String getDestination() {
        return destination;
    }

    /** Sets the destination of a trip. */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /** Gets the start of a trip in seconds. */
    public int getFrom() {
        return from;
    }

    /** Sets the start of a trip in iseconds. */
    public void setFrom(int from) {
        this.from = from;
    }

    /** Gets the end of a trip in seconds. */
    public int getTo() {
        return to;
    }

    /** Sets the end of a trip in seconds. */
    public void setTo(int to) {
        this.to = to;
    }

    public ArrayList<TMember> getMembers(TravelAppDataSource dataSource, boolean forceFromDB) {
        if (dataSource != null && (forceFromDB || members == null)) {
            members = new ArrayList<>();
            TMember member = new TMember();
            TTripMember tripMember = new TTripMember();
            String sql = "SELECT * FROM " +  member.getTable() + " "
                    + "WHERE " + TMember.CN_ID + " IN "
                    + "(SELECT " + TTripMember.CN_MEMBER_ID + " FROM " + tripMember.getTable() + " "
                    + "WHERE " + TTripMember.CN_TRIP_ID + "=?)";
            String[] params = new String[]{getIdStr()};
            ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);
            if (rows != null) {
                for (ContentValues values : rows) {
                    members.add((TMember)member.createObjWithContentVals(values));
                }
            }
        }
        return members;
    }

    public ArrayList<TCostrecord> getCostrecords(TravelAppDataSource dataSource, boolean forceFromDB){
        if (dataSource != null && (forceFromDB || costrecords == null)){
            costrecords = new ArrayList<>();
            TCostrecord costrecord = new TCostrecord();
            TTrip trip = new TTrip();
            String sql = "SELECT * FROM " + costrecord.getTable() + " "
                    + "WHERE " + TCostrecord.CN_TRIP_ID + " = ?";
            String[] params = new String[]{getIdStr()};
            ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);
            if (rows != null) {
                for (ContentValues values : rows) {
                    costrecords.add((TCostrecord)costrecord.createObjWithContentVals(values));
                }
            }
        }
        return costrecords;
    }

    public TPackinglist getPackinglist(TravelAppDataSource dataSource){
        if (dataSource != null) {
            TPackinglist list = new TPackinglist();
            list.setId(packinglistId);
            return dataSource.selectObj(list);
        }
        return null;
    }

    public void addMemberIfNeeded(TravelAppDataSource dataSource, TMember member) {
        member = dataSource.createOrUpdateObj(member);
        TTripMember tripMember = new TTripMember();
        String sql = "SELECT * FROM " + tripMember.getTable() + " ";
        sql += "WHERE " + TTripMember.CN_TRIP_ID + "=? ";
        sql += "AND " + TTripMember.CN_MEMBER_ID + "=? ";
        String[] params = new String[] { getIdStr(), member.getIdStr() };
        ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);

        if (rows == null || rows.size() == 0) {
            tripMember.setTripId(getId());
            tripMember.setMemberId(member.getId());
            dataSource.createOrUpdateObj(tripMember);
            getMembers(dataSource, true);
        }

    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_PACKINGLIST_ID, getPackinglistId());
        cv.put(CN_NAME          , getName());
        cv.put(CN_DESTINATION   , getDestination());
        cv.put(CN_FROM          , getFrom());
        cv.put(CN_TO            , getTo());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TTrip obj = new TTrip();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setPackinglistId(DictionaryHelper.getInt(values, CN_PACKINGLIST_ID, INVALID_ID));
        obj.setName(DictionaryHelper.getString(values, CN_NAME, ""));
        obj.setDestination(DictionaryHelper.getString(values, CN_DESTINATION, ""));
        obj.setFrom(DictionaryHelper.getInt(values, CN_FROM, 0));
        obj.setTo(DictionaryHelper.getInt(values, CN_TO, 0));
        return obj;
    }

    @Override
    public String toString() {
        return this.getId() + " - " + name + " - " + getMembers(TravelAppDataSource.getApplicationDatasource(), false).size() + " members";
    }
}
