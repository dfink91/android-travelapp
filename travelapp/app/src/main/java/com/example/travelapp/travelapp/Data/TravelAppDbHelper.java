package com.example.travelapp.travelapp.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by danilofink on 12/11/17.
 * For concern of how to add things to this DbHelper, check
 * ScheduleDatabase.java of the Github project google/iosched
 * https://github.com/google/iosched/blob/e4364eec0d8f358215aee30cce2b4836ec169b5a/lib/src/main/java/com/google/samples/apps/iosched/provider/ScheduleDatabase.java
 */
public class TravelAppDbHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = TravelAppDbHelper.class.getSimpleName();

    /** The name of the database. */
    public static final String DATABASE_NAME = "travels.db";

    //Versions
    public static final int VER_2017_INITIAL = 1;

    /** Current version. */
    private static final int CUR_DATABASE_VERSION = VER_2017_INITIAL;

    private final Context mContext;

    /** Constructor. */
    public TravelAppDbHelper(Context context) {
        super(context, DATABASE_NAME, null, CUR_DATABASE_VERSION);
        mContext = context;
    }

    /**
     * Holds all the table names.
     * Also holds deprecated table names.
     */
    interface Tables {
        String TRIPS = "trips";
        String MEMBERS = "members";
        String TRIP_MEMBER = "trip_member";
        String COSTRECORDS = "costrecords";
        String RECORDDETAILS = "recorddetails";
        String TRAVELDOC_GROUPS = "traveldoc_groups";
        String TRAVELDOCS = "traveldocs";
        String PACKINGLISTS = "packinglists";
        String PACKINGLIST_ITEMS = "packinglist_items";
        String ITEMS = "items";
        String CATEGORIES = "categories";
        String TAGS = "tags";
        String ITEM_CATEGORY = "item_category";
        String ITEM_TAGS = "item_tags";

        // When tables get deprecated, add them to this list (so they get correctly deleted
        // on database upgrades).
        enum DeprecatedTables {
            DELETED_TABLE1("deleted_table1"),
            DELETED_TABLE2("deleted_table2");

            String tableName;

            DeprecatedTables(String tableName) {
                this.tableName = tableName;
            }
        }
    }

    /** {@code REFERENCES} clauses. */
    public interface References {
        String TRIP_ID          = "REFERENCES " + Tables.TRIPS + "(" + TTrip.CN_ID + ")";
        String MEMBER_ID        = "REFERENCES " + Tables.MEMBERS + "(" + TMember.CN_ID + ")";
        String COSTRECORD_ID    = "REFERENCES " + Tables.COSTRECORDS + "(" + TCostrecord.CN_ID + ")";
        String TRAVELDOC_GROUP_ID = "REFERENCES " + Tables.TRAVELDOC_GROUPS + "(" + TTravelDocGroup.CN_ID + ")";
        String PACKINGLIST_ID   = "REFERENCES " + Tables.PACKINGLISTS + "(" + TPackinglist.CN_ID + ")";
        String ITEM_ID          = "REFERENCES " + Tables.ITEMS + "(" + TItem.CN_ID + ")";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(getCreateStatement(Tables.ITEMS              , new TItem().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.PACKINGLISTS       , new TPackinglist().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.PACKINGLIST_ITEMS  , new TPackinglistItem().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.MEMBERS            , new TMember().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.TRIPS              , new TTrip().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.TRIP_MEMBER        , new TTripMember().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.COSTRECORDS        , new TCostrecord().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.RECORDDETAILS      , new TRecorddetail().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.TRAVELDOC_GROUPS   , new TTravelDocGroup().getColumnDefinitions()));
            db.execSQL(getCreateStatement(Tables.TRAVELDOCS         , new TTravelDoc().getColumnDefinitions()));
        }
        catch (Exception ex) {
            Log.e(LOG_TAG, "Fehler beim Anlegen der Tabelle: " + ex.getMessage());
        }
    }

    /** Creates the create statement for a given table name and column definitions. */
    private String getCreateStatement(String tableName, Column[] definitions) {
        String sqlCreate = "CREATE TABLE " + tableName + " (";
        sqlCreate += getSqlDefinitionLine(definitions[0]);
        for (int i = 1, cnt = definitions.length; i < cnt; i++) {
            sqlCreate += "," + getSqlDefinitionLine(definitions[i]);
        }
        sqlCreate += ")";
        return sqlCreate;
    }

    /** Generates the definition of a single column definition for the create statement. */
    private static String getSqlDefinitionLine(Column def) {
        return def.getName() + " " + def.getDbDefinition();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
