package com.example.travelapp.travelapp.Components;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;

import com.example.travelapp.travelapp.Helper.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by danilofink on 11/01/18.
 */
public class EditDate extends android.support.v7.widget.AppCompatEditText
        implements DatePickerDialog.OnDateSetListener, View.OnClickListener{

    private Calendar myCalendar;
    private Date date;

    public EditDate(Context context) {
        this(context, null);
    }

    public EditDate(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.editTextStyle);
    }

    public EditDate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.myCalendar = Calendar.getInstance();
        setEmptyDate();
        setOnClickListener(this);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        setDate(myCalendar.getTime());
        updateText();
    }

    private void updateText() {
        if (date.getTime() > 0) {
            String myFormat = "dd. MMM. yy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Util.getCurrentLocale(this.getContext()));

            this.setText(sdf.format(myCalendar.getTime()));
        } else
            this.setText(null);
        this.setError(null);
    }

    @Override
    public void onClick(View v) {
        DatePickerDialog dpd = new DatePickerDialog(this.getContext(), this, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dpd.show();
    }

    public void setEmptyDate() {
        setDate(0);
    }

    public void setDateSeconds(int seconds) {
        setDate(seconds * 1000L);
    }

    public void setDate(long date) {
        setDate(new Date(date));
    }

    public void setDate(Date date) {
        this.date = date != null ? date : new Date(0);
        if (this.date.getTime() > 0)
            myCalendar.setTime(this.date);
        else
            myCalendar = Calendar.getInstance();
        updateText();
    }

    public Date getDate() {
        return date;
    }

    public int getDateSeconds() {
        return Util.getSeconds(date.getTime());
    }
}
