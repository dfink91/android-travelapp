package com.example.travelapp.travelapp.Data;

/**
 * Created by danilofink on 20/11/17.
 */
public class Column {
    private String name;
    private String dbDefinition;

    /** Constructor. */
    public Column(String name, String dbDefinition) {
        this.name = name;
        this.dbDefinition = dbDefinition;
    }

    /** Gets the name of a column. */
    public String getName() {
        return name;
    }

    /** Sets the name of a column. */
    public void setName(String name) {
        this.name = name;
    }

    /** Gets the definition of a column. */
    public String getDbDefinition() {
        return dbDefinition;
    }

    /** Sets the definition of a column. */
    public void setDbDefinition(String dbDefinition) {
        this.dbDefinition = dbDefinition;
    }
}
