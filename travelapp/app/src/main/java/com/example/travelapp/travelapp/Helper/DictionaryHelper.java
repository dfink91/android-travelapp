package com.example.travelapp.travelapp.Helper;

import android.content.ContentValues;

/**
 * Created by danilofink on 20/11/17.
 */

public abstract class DictionaryHelper {

    public static Integer getInt(ContentValues values, String key, int defaultVal) {
        if (values.containsKey(key))
            defaultVal = values.getAsInteger(key);
        return defaultVal;
    }

    public static Float getFloat(ContentValues values, String key, float defaultVal) {
        if (values.containsKey(key))
            defaultVal = values.getAsFloat(key);
        return defaultVal;
    }

    public static String getString(ContentValues values, String key, String defaultVal) {
        if (values.containsKey(key) && values.getAsString(key) != null)
            defaultVal = values.getAsString(key).trim();
        return defaultVal;
    }
}
