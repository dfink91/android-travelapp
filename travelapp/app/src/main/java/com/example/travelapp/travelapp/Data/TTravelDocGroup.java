package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

import java.util.ArrayList;

/**
 * Created by danilofink on 07/01/18.
 */
public class TTravelDocGroup extends TBaseDbObject {

    // Columndefinition
    public static final String CN_TRIP_ID       = "trip_id";
    public static final String CD_TRIP_ID       = SCD_INT + " " + TravelAppDbHelper.References.TRIP_ID;
    public static final String CN_TITLE          = "title";
    public static final String CD_TITLE          = SCD_TEXT_NOT_NULL;
    public static final String CN_SHARED        = "shared";
    public static final String CD_SHARED        = SCD_BOOL;

    // Membervariables
    private long tripId;
    private String title;
    private boolean shared;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.TRAVELDOC_GROUPS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_TRIP_ID, CD_TRIP_ID),
                new Column(CN_TITLE, CD_TITLE),
                new Column(CN_SHARED, CD_SHARED)
        };
    }

    /** Empty Constructor. */
    public TTravelDocGroup() {
        setDefaultIdsAndReferences();
    }

    /** Constructor. */
    public TTravelDocGroup(TTrip trip, String title) {
        setDefaultIdsAndReferences();
        setTitle(title);
        setTripId(trip.getId());
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setTripId(INVALID_ID);
    }

    /** Gets the trip id. */
    public long getTripId() {
        return tripId;
    }

    /** Sets the trip id. */
    public void setTripId(long val) {
        this.tripId = val;
    }

    /** Gets the title. */
    public String getTitle() { return title; }

    /** Sets the title. */
    public void setTitle(String val) { this.title = val; }

    /** Tells if a traveldoc_group is shared. */
    public boolean isShared() { return shared; }

    /** Sets if the traveldoc_group is shared. */
    public void setShared(boolean val) { this.shared = val; }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_TRIP_ID   , getTripId());
        cv.put(CN_TITLE     , getTitle());
        cv.put(CN_SHARED    , isShared());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TTravelDocGroup obj = new TTravelDocGroup();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setTripId(DictionaryHelper.getInt(values, CN_TRIP_ID, INVALID_ID));
        obj.setTitle(DictionaryHelper.getString(values, CN_TITLE, ""));
        obj.setShared(values.getAsBoolean(CN_SHARED));
        return obj;
    }

    public static ArrayList<TTravelDocGroup> getAllForTrip(TravelAppDataSource dataSource, TTrip holder) {
        ArrayList<TTravelDocGroup> list = new ArrayList<>();
        if (dataSource != null && holder != null) {
            TTravelDocGroup obj = new TTravelDocGroup();
            list = dataSource.getAllWhere(TTravelDocGroup.CN_TRIP_ID, holder.getIdStr(), obj);
        }
        return list;
    }
}
