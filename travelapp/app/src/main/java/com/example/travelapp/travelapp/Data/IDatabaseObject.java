package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;
import android.provider.BaseColumns;

/**
 * Created by danilofink on 15/11/17.
 */
public interface IDatabaseObject extends BaseColumns {

    // Common and default values
    int INVALID_ID = -1;

    // Helpers for standard column definitions (prefix SCD)
    String SCD_PRIMARY_KEY_AUTOINC = "INTEGER PRIMARY KEY AUTOINCREMENT";
    String SCD_TEXT = "TEXT";
    String SCD_TEXT_NOT_NULL = "TEXT NOT NULL";
    String SCD_INT_NOT_NULL = "INTEGER NOT NULL";
    String SCD_INT = "INTEGER";
    String SCD_FLOAT_NOT_NULL = "REAL NOT NULL";
    String SCD_FLOAT = "REAL";
    String SCD_BOOL = "INTEGER";

    /** Gets the name of a table. */
    String getTable();

    /** Delivers an array of the column definitions of a table. */
    Column[] getColumnDefinitions();

    /** Delivers an array of the column names of a table. */
    default String[] getColumns() {
        Column[] cols = getColumnDefinitions();
        int cnt = cols.length;
        String[] colStrs = new String[cnt];
        for (int i = 0; i < cnt; i++) {
            colStrs[i] = cols[i].getName();
        }
        return colStrs;
    }

    /** Gets the values of an Object as ContentValues (Dictionary). */
    ContentValues getContentValues(boolean includePrimaryKey);

    /** Creates an Object of this type with given ContentValues. */
    IDatabaseObject createObjWithContentVals(ContentValues values);
}
