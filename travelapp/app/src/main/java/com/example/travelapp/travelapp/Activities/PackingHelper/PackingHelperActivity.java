package com.example.travelapp.travelapp.Activities.PackingHelper;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TItem;
import com.example.travelapp.travelapp.Data.TPackinglist;
import com.example.travelapp.travelapp.Data.TPackinglistItem;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilofink on 03/01/18.
 */
public class PackingHelperActivity extends MenuActivity {
    private static final String LOG_TAG = PackingHelperActivity.class.getSimpleName();

    EditText txtAddItem;
    ListView listView;
    TPackinglist packinglist;
    ArrayList<TPackinglistItem> pItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_packing_helper);

        setupComponents();
        setupButtons();
    }
    @Override
    protected void onStart() {
        super.onStart();
        refreshPackinglist();
    }

    private void setupComponents() {
        txtAddItem = (EditText) findViewById(R.id.txt_packing_item);
        listView = (ListView) findViewById(R.id.listview_packing_list);
    }

    private void refreshPackinglist() {
        if (packinglist == null) {
            if((packinglist = getCurrentTrip().getPackinglist(dataSource)) == null) {
                packinglist = new TPackinglist();
                dataSource.createOrUpdateObj(packinglist);
            }
        }
        if (packinglist != null) {
            pItems = packinglist.getAllItems(dataSource);
            listView.setAdapter(new PackingItemRowAdapter(this, pItems));
        }
    }

    public void onBtnAddItem(View v) {
        String val = txtAddItem.getText().toString();
        if(!TextUtils.isEmpty(val) && packinglist != null) {
            TItem item = new TItem(val);
            item.saveInDB(dataSource);
            TPackinglistItem tpi = new TPackinglistItem(packinglist.getId(), item.getId());
            tpi.saveInDB(dataSource);

            txtAddItem.setText(null);
            refreshPackinglist();
        }
        else
            txtAddItem.setError(getString(R.string.editText_error_msg_empty));
    }

    private void setupButtons() {
        Button btn = (Button) findViewById(R.id.button_add_item);
        btn.setOnClickListener(this::onBtnAddItem);
    }

}
