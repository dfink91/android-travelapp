package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

/**
 * Created by danilofink on 20/11/17.
 */
public class TMember extends TBaseDbObject {

    // Columndefinition
    public static final String CN_NAME          = "name";
    public static final String CD_NAME          = SCD_TEXT_NOT_NULL;

    // Membervariables
    private String name;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.MEMBERS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_NAME, CD_NAME)
        };
    }

    /** Empty Constructor. */
    public TMember() { setDefaultIdsAndReferences(); }

    /** Empty Constructor. */
    public TMember(String name) {
        setDefaultIdsAndReferences();
        setName(name);
    }

    /** Gets the name. */
    public String getName() {
        return name;
    }

    /** Sets the name.  */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_NAME          , getName());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TMember obj = new TMember();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setName(DictionaryHelper.getString(values, CN_NAME, ""));
        return obj;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public String getDisplayName() {
        return getName();
    }

    @Override
    public void setDisplayName(String val) {
        setName(val);
    }
}
