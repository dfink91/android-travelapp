package com.example.travelapp.travelapp.Activities.CostManagement;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.travelapp.travelapp.Components.EditCurrency;
import com.example.travelapp.travelapp.Components.RefreshActivityListener;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danilo on 13.01.18.
 */


public class CostOverviewRowAdapter extends ArrayAdapter {

    List<TCostrecord> dataList;
    Context context;
    TravelAppDataSource dataSource;
    TextView tv_title;
    EditCurrency editCurrency_totalAmount;
    ImageButton btn_delete;
    ImageButton btn_edit;

    ArrayList<RefreshActivityListener> refreshActivityListener;


    public CostOverviewRowAdapter(@NonNull Context context, List<TCostrecord> resource)
    {
        super(context, R.layout.row_costoverview,resource);
        this.context = context;
        dataList = new ArrayList<>();
        this.dataList = resource;
        dataSource = new TravelAppDataSource(context);
        refreshActivityListener = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.row_costoverview, parent, false);

        tv_title = convertView.findViewById(R.id.textView_costTitle);
        tv_title.setText(dataList.get(position).getName());
        editCurrency_totalAmount = convertView.findViewById(R.id.editCurrency_CostTotalAmount);
        editCurrency_totalAmount.setEnabled(false);
        editCurrency_totalAmount.setTextAmount(dataList.get(position).getTotalAmount(dataSource, true));
        btn_delete = convertView.findViewById(R.id.btn_delete_costEntry);
        btn_edit = convertView.findViewById(R.id.btn_edit_costEntry);

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AskOption(position).show();
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCostRecord(position);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCostRecord(position);
            }
        });

        return convertView;
    }

    private AlertDialog AskOption(int position)
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getContext())
                .setTitle("Delete")
                .setMessage("Do you want to delete?")
                .setIcon(R.drawable.ic_close_black_24dp)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        TCostrecord tempRecord = dataList.get(position);
                        ArrayList<TRecorddetail> tempList = tempRecord.getRecordDetails(dataSource, true);
                        for (TRecorddetail tdetail: tempList) {
                            dataSource.deleteObj(tdetail);
                        }
                        dataSource.deleteObj(tempRecord);
                        for (RefreshActivityListener rl : refreshActivityListener)
                            rl.onRefreshRequested();
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    public void addOnRefreshRequestedListener(RefreshActivityListener rl) {
        refreshActivityListener.add(rl);
    }

    public void removeOnRefreshRequestedListener(RefreshActivityListener rl) {
        refreshActivityListener.remove(rl);
    }

    private void openCostRecord(int position) {
        Intent intent = new Intent(context, AddCostActivity.class);
        intent.putExtra(AddCostActivity.SELECTED_COST_RECORD, dataList.get(position).getId());
        context.startActivity(intent);
    }
}
