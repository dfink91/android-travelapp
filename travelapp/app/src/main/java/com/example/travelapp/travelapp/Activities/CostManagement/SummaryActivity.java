package com.example.travelapp.travelapp.Activities.CostManagement;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.R;

import java.util.ArrayList;

/**
 * Created by danilo on 25.01.18.
 */


public class SummaryActivity extends MenuActivity {

    public final static String SELECTED_COST_RECORD = "SELECTED_COST_RECORD";

    //Components
    ListView lv_Summary;

    //Class variables
    ArrayList<TCostrecord> list_cost_records;
    ArrayList<TRecorddetail> allDetailsList;
    ArrayList<CostSummary> summaryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavBarNoSideMenu();
        setContentView(R.layout.content_cost_summary);

        summaryList = CostSummary.calcCostSummary(dataSource, getCurrentTrip());

        ArrayAdapter<CostSummary> arrayAdapter = new ArrayAdapter<>(this, R.layout.row_simple, summaryList);
        lv_Summary = (ListView) findViewById(R.id.listView_Summary);
        lv_Summary.setAdapter(arrayAdapter);
    }
}
