package com.example.travelapp.travelapp.Activities.BaseActivities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.travelapp.travelapp.R;

/**
 * Created by danilofink on 07/01/18.
 */
public abstract class DialogActivity extends MenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavBarDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dialog_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = true;
        switch (item.getItemId()) {
            case R.id.action_save:
                if (saveObject())
                    onBackPressed();
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    /**
     * Holds the instructions to carry out after the Save Button
     * of the DialogActivity has been pressed.
     * @return true if instructions have been successful and dialog can be closed.
     */
    protected abstract boolean saveObject();
}
