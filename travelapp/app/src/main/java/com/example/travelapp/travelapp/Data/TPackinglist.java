package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

import java.util.ArrayList;

/**
 * Created by danilofink on 20/11/17.
 */
public class TPackinglist extends TBaseDbObject {

    // Columndefinition
    public static final String CN_NAME          = "name";
    public static final String CD_NAME          = SCD_TEXT_NOT_NULL;

    // Membervariables
    private String name;

    // Temporary variables
    private ArrayList<TPackinglistItem> pItems;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.PACKINGLISTS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_NAME, CD_NAME),
        };
    }

    /** Empty Constructor. */
    public TPackinglist() {
        setDefaultIdsAndReferences();
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
    }

    /** Gets the name. */
    public String getName() {
        return name;
    }

    /** Sets the name.  */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_NAME          , getName());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TPackinglist obj = new TPackinglist();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setName(DictionaryHelper.getString(values, CN_NAME, ""));
        return obj;
    }

    public ArrayList<TPackinglistItem> getAllItems(TravelAppDataSource dataSource) {
         if (dataSource != null) {
            pItems = new ArrayList<>();
            TPackinglistItem pli = new TPackinglistItem();
            String sql = "SELECT * FROM " + pli.getTable() + " "
                    + "WHERE " + TPackinglistItem.CN_PACKINGLIST_ID + "= ?";
            String[] params = new String[]{getIdStr()};
            ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);
            if (rows != null) {
                for (ContentValues values : rows) {
                    pItems.add((TPackinglistItem) pli.createObjWithContentVals(values));
                }
            }
        }
        return pItems;
    }
}
