package com.example.travelapp.travelapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.travelapp.travelapp.Activities.Home.HomeActivity;
import com.example.travelapp.travelapp.R;

import static java.lang.Thread.sleep;

/**
 * Created by danilo on 30.01.18.
 */

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
