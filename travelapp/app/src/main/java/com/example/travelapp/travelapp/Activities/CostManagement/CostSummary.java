package com.example.travelapp.travelapp.Activities.CostManagement;

import android.support.annotation.NonNull;

import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Data.TMember;
import com.example.travelapp.travelapp.Data.TRecorddetail;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.Helper.Util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by danilo on 27.01.18.
 */

public class CostSummary implements Comparable<CostSummary> {
    private TMember member;
    private float debt;
    private float credit;
    private float balance;

    public CostSummary(TMember member) {
        this.member = member;
    }

    public TMember getMember() {
        return member;
    }

    public float getDebt() {
        return debt;
    }

    public float getCredit() {
        return credit;
    }

    public float getBalance() {
        return this.debt - this.credit;
    }
    
    public void addDebt(float newDebt) {
        this.debt += newDebt;
    }
    
    public void addCredit(float newCredit) {
        this.credit += newCredit;
    }

    @Override
    public String toString() {
        String s;
        if(getBalance() >= 0)
            s = getMember().getName() + " owes the group:\n" + Util.convertToCurrency(getBalance(), ".", "€");
        else
            s = getMember().getName() + " gets from the group:\n" + Util.convertToCurrency(getBalance(), ".", "€");
        return s;
    }

    public static ArrayList<CostSummary> calcCostSummary(TravelAppDataSource dataSource, TTrip trip) {
        ArrayList<CostSummary> summaryList = new ArrayList<>();
        ArrayList<TMember> memberArrayList = trip.getMembers(dataSource, true);

        for(TMember tMember : memberArrayList) {
            CostSummary cs = new CostSummary(tMember);
            int index = Collections.binarySearch(summaryList, cs);
            if (index < 0)
                summaryList.add(Math.abs(index + 1), cs);
        }

        ArrayList<TCostrecord> list_cost_records = trip.getCostrecords(dataSource, true);
        for(TCostrecord cr : list_cost_records) {
            TMember p = new TMember();
            p.setId(cr.getMemberId());
            p = dataSource.selectObj(p);
            ArrayList<TRecorddetail> allDetailsList = cr.getRecordDetails(dataSource, true);
            float sum = 0f;
            for (TRecorddetail td : allDetailsList) {
                TMember b = new TMember();
                b.setId(td.getMemberId());
                b = dataSource.selectObj(b);
                sum += td.getAmount();
                getCostSummaryOfMember(summaryList, b).addDebt(td.getAmount());
            }
            getCostSummaryOfMember(summaryList, p).addCredit(sum);
        }
        return summaryList;
    }

    private static CostSummary getCostSummaryOfMember(ArrayList<CostSummary> list, TMember m) {
        int index = Collections.binarySearch(list, new CostSummary(m));
        CostSummary cs = null;
        if (index >= 0)
            cs = list.get(index);
        return cs;
    }

    @Override
    public int compareTo(@NonNull CostSummary other) {
        return (int)(this.getMember().getId() - other.getMember().getId());
    }
}
