package com.example.travelapp.travelapp.Activities.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.R;
import com.example.travelapp.travelapp.ScoccoApplication;

import java.util.List;

/**
 * Created by danilofink on 16/01/18.
 */
public class TripsActivity extends MenuActivity {
    private static final String LOG_TAG = TripsActivity.class.getSimpleName();

    private ListView listTrips;
    private List<TTrip> tripList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNavBarNoSideMenu();

        setContentView(R.layout.content_trips);
        setupButtons();
        setupComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        showAllListEntries();
    }

    private void setupComponents() {
        listTrips = (ListView) findViewById(R.id.listview_trips);
        listTrips.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TTrip trip = (TTrip) parent.getItemAtPosition(position);
                finishWithTripId(TripsActivity.this, trip.getId());
            }
        });
    }

    private void showAllListEntries () {
        tripList = dataSource.getAll(new TTrip());

        ArrayAdapter<TTrip> tripArrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                R.layout.row_simple,
                tripList);

        ListView tripsListView = (ListView) findViewById(R.id.listview_trips);
        tripsListView.setAdapter(tripArrayAdapter);
    }

    public void onBtnNewTrip(View v) {
        startActivityForResult(new Intent(TripsActivity.this, TripActivity.class), 0);
    }

    private void setupButtons() {
        Button btn = (Button) findViewById(R.id.btn_new_trip);
        btn.setOnClickListener(this::onBtnNewTrip);
    }

    private static void finishWithTripId(Activity activity, long id) {
        //---set the data to pass back---
        Intent data = new Intent();
        data.putExtra(TripActivity.SELECTED_TRIP, id);
        activity.setResult(RESULT_OK, data);
        //---close the activity---
        activity.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            finishWithTripId(this, data.getLongExtra(TripActivity.SELECTED_TRIP, TTrip.INVALID_ID));
    }
}
