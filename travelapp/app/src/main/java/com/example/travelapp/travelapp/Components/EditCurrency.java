package com.example.travelapp.travelapp.Components;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

import com.example.travelapp.travelapp.Activities.CostManagement.AddCostActivity;
import com.example.travelapp.travelapp.Helper.Util;

import java.util.ArrayList;

/**
 * Created by danilo on 13.01.18.
 */

public class EditCurrency extends android.support.v7.widget.AppCompatEditText {

    ArrayList<AmountListener> amountListeners;
    float amount;

    public EditCurrency(Context context) {
        this(context, null);
    }

    public EditCurrency(Context context, AttributeSet attrs) {
        this(context, attrs,  android.support.v7.appcompat.R.attr.editTextStyle);
    }

    public EditCurrency(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        amountListeners = new ArrayList<>();
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
            this.amount = amount;
            for (AmountListener al : amountListeners)
                al.onAmountChanged(this.amount);
    }

    public void setTextAmount(float amount) {
        if (amount != 0)
            setText(Util.convertToCurrency(amount, ".", ""));
        else
            setText(null);
        setAmount(amount);
    }

    public void addAmountListener(AmountListener al) {
        amountListeners.add(al);
    }

    public void removeAmountListener(AmountListener al) {
        amountListeners.remove(al);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (!focused) {
            String input = getText().toString();
            if (!Util.isEmpty(input))
                setTextAmount(Float.parseFloat(input));
            else
                setAmount(0);
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        String input = getText().toString();
        if (!Util.isEmpty(input)) {
            Float totalAmount;
            totalAmount = Float.parseFloat(input);
            setAmount(totalAmount);
        }
    }
}