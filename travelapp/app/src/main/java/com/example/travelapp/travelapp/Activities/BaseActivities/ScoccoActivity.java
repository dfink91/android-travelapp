package com.example.travelapp.travelapp.Activities.BaseActivities;

import android.app.Activity;
import android.app.Application;
import android.support.v7.app.AppCompatActivity;

import com.example.travelapp.travelapp.Data.TTrip;
import com.example.travelapp.travelapp.ScoccoApplication;

/**
 * Created by danilofink on 05/01/18.
 */
public abstract class ScoccoActivity extends AppCompatActivity {

    public TTrip getCurrentTrip() {
        Application app = getApplication();
        if (app instanceof ScoccoApplication)
            return ((ScoccoApplication) app).getCurrentTrip();
        return null;
    }
}
