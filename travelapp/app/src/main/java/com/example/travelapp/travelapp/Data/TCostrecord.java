package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;
import com.example.travelapp.travelapp.Helper.Util;

import java.util.ArrayList;

/**
 * Created by danilofink on 20/11/17.
 */
public class TCostrecord extends TBaseDbObject {

    // Columndefinition
    public static final String CN_MEMBER_ID     = "member_id";
    public static final String CD_MEMBER_ID     = SCD_INT + " " + TravelAppDbHelper.References.MEMBER_ID;
    public static final String CN_TRIP_ID       = "trip_id";
    public static final String CD_TRIP_ID       = SCD_INT + " " + TravelAppDbHelper.References.TRIP_ID;
    public static final String CN_NAME          = "name";
    public static final String CD_NAME          = SCD_TEXT_NOT_NULL;
    public static final String CN_DESCRIPTION   = "description";
    public static final String CD_DESCRIPTION   = SCD_TEXT;
    public static final String CN_DATE          = "date";
    public static final String CD_DATE          = SCD_INT;
    public static final String CN_FILELINK      = "filelink";
    public static final String CD_FILELINK      = SCD_TEXT;

    // Membervariables
    private long memberId;
    private long tripId;
    private String name;
    private String description;
    private int date;
    private String filelink;

    // Temorary Variables
    private ArrayList<TRecorddetail> recorddetails;
    private float totalAmount;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.COSTRECORDS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_MEMBER_ID, CD_MEMBER_ID),
                new Column(CN_TRIP_ID, CD_TRIP_ID),
                new Column(CN_NAME, CD_NAME),
                new Column(CN_DESCRIPTION, CD_DESCRIPTION),
                new Column(CN_DATE, CD_DATE),
                new Column(CN_FILELINK, CD_FILELINK)
        };
    }

    /** Empty Constructor. */
    public TCostrecord() {
        this(INVALID_ID);
    }

    public TCostrecord(long costRecordId) {
        setDefaultIdsAndReferences();
        setId(costRecordId);
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setMemberId(INVALID_ID);
        setTripId(INVALID_ID);
    }

    /** Gets the member id. */
    public long getMemberId() {
        return memberId;
    }

    /** Sets the member id. */
    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    /** Gets the trip id. */
    public long getTripId() {
        return tripId;
    }

    /** Sets the trip id. */
    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    /** Gets the name. */
    public String getName() {
        return name;
    }

    /** Sets the name.  */
    public void setName(String name) {
        this.name = name;
    }

    /** Gets the description. */
    public String getDescription() {
        return description;
    }

    /** Sets the description. */
    public void setDescription(String description) {
        this.description = description;
    }

    /** Gets the date in seconds. */
    public int getDate() {
        return date;
    }

    /** Sets the date in seconds. */
    public void setDate(int date) {
        this.date = date;
    }

    /** Gets the filelink to an associated file. */
    public String getFilelink() {
        return filelink;
    }

    /** Sets the filelink to an associated file. */
    public void setFilelink(String filelink) {
        this.filelink = filelink;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_MEMBER_ID     , getMemberId());
        cv.put(CN_TRIP_ID     , getTripId());
        cv.put(CN_NAME          , getName());
        cv.put(CN_DESCRIPTION   , getDescription());
        cv.put(CN_DATE          , getDate());
        cv.put(CN_FILELINK      , getFilelink());
        return cv;
    }

    public ArrayList<TRecorddetail> getRecordDetails(TravelAppDataSource dataSource, boolean forceFromDB){
        if (dataSource != null && (forceFromDB || recorddetails == null)){
            recorddetails = new ArrayList<>();
            TRecorddetail recorddetail = new TRecorddetail();
            String sql = "SELECT * FROM " + recorddetail.getTable() + " "
                    + "WHERE " + TRecorddetail.CN_RECORD_ID + " = ?";
            String[] params = new String[]{getIdStr()};
            ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);
            if (rows != null) {
                for (ContentValues values : rows) {
                    recorddetails.add((TRecorddetail) recorddetail.createObjWithContentVals(values));
                }
            }
        }
        return recorddetails;
    }

    public float getTotalAmount(TravelAppDataSource dataSource, boolean forceFromDB){
        if (dataSource != null && (forceFromDB || totalAmount == 0)){
            TRecorddetail recorddetail = new TRecorddetail();
            String columnName = "SUM(" + TRecorddetail.CN_AMOUNT + ")";
            String sql = "SELECT " + columnName + " FROM " + recorddetail.getTable() + " "
                    + "WHERE " + TRecorddetail.CN_RECORD_ID + " = ?";
            String[] params = new String[]{getIdStr()};
            ArrayList<ContentValues> rows = dataSource.rawQuery(sql, params);
            if (Util.size(rows) == 1) {
                ContentValues val = rows.get(0);
                if (val.get(columnName) != null)
                    totalAmount = val.getAsFloat(columnName);
            }
        }
        return totalAmount;
    }



    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TCostrecord obj = new TCostrecord();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setMemberId(DictionaryHelper.getInt(values, CN_MEMBER_ID, INVALID_ID));
        obj.setTripId(DictionaryHelper.getInt(values, CN_TRIP_ID, INVALID_ID));
        obj.setName(DictionaryHelper.getString(values, CN_NAME, ""));
        obj.setDescription(DictionaryHelper.getString(values, CN_DESCRIPTION, ""));
        obj.setDate(DictionaryHelper.getInt(values, CN_DATE, 0));
        obj.setFilelink(DictionaryHelper.getString(values, CN_FILELINK, ""));
        return obj;
    }

    @Override
    public String toString() {
        return getName().toString() + " " + getTotalAmount(TravelAppDataSource.getApplicationDatasource(), true);
    }
}
