package com.example.travelapp.travelapp.Activities.Wallet;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.travelapp.travelapp.Data.TTravelDoc;
import com.example.travelapp.travelapp.Data.TTravelDocGroup;
import com.example.travelapp.travelapp.Data.TravelAppDataSource;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.Helper.UtilExternal;
import com.example.travelapp.travelapp.R;

import java.net.URI;
import java.util.ArrayList;

/**
 * Created by danilofink on 06/01/18.
 */
public class TravelDocGroupRowAdapter extends BaseAdapter {

    private Context context;
    private TravelAppDataSource dataSource;
    private ArrayList<TTravelDocGroup> data;
    private static LayoutInflater inflater = null;

    public TravelDocGroupRowAdapter(Context context, ArrayList<TTravelDocGroup> data) {
        this.context = context;
        dataSource = new TravelAppDataSource(context);
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Util.size(data);
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.row_travel_doc, null);
        TextView txtTitle = (TextView) vi.findViewById(R.id.txt_title);
        TextView txtInfo = (TextView) vi.findViewById(R.id.txt_info);
        TTravelDocGroup tdGroup = data.get(position);
        txtTitle.setText(tdGroup.getTitle());

        ArrayList<TTravelDoc> list = TTravelDoc.getAllForGroup(dataSource, tdGroup);
        String filePreview = null;
        for (TTravelDoc td : list) {
            if (!Util.isEmpty(td.getFilelink())) {
                filePreview = td.getFilelink();
                break;
            }
        }
        ImageView imgDoc = (ImageView) vi.findViewById(R.id.img_view_preview);
        if (!Util.isEmpty(filePreview)) {
            UtilExternal.setImageViewWithUri(context, imgDoc, Uri.parse(filePreview));
            imgDoc.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else {
            imgDoc.setImageResource(R.drawable.ic_add_documents_black_24dp);
            imgDoc.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }

        txtInfo.setText(Util.size(list) + " - items");
        return vi;
    }
}
