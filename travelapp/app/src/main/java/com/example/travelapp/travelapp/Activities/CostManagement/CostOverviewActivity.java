package com.example.travelapp.travelapp.Activities.CostManagement;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.travelapp.travelapp.Activities.BaseActivities.MenuActivity;
import com.example.travelapp.travelapp.Components.RefreshActivityListener;
import com.example.travelapp.travelapp.Data.TCostrecord;
import com.example.travelapp.travelapp.Helper.Util;
import com.example.travelapp.travelapp.R;

import java.util.List;

public class CostOverviewActivity extends MenuActivity implements RefreshActivityListener {

    private List<TCostrecord> list_cost_records;
    private ListView listView_costentries;
    CostOverviewRowAdapter overviewRowAdapter;
    ArrayAdapter<String> defaultAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_cost_overview);

        Button btn_add = (Button) findViewById(R.id.btn_addCost);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CostOverviewActivity.this, AddCostActivity.class);
                startActivity(intent);
            }
        });

        Button btn_summary = (Button) findViewById(R.id.btn_costSummary);
        btn_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CostOverviewActivity.this, SummaryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupComponents();
    }

    @Override
    public void onRefreshRequested() {
        setupComponents();
    }

    public void setupComponents(){
        list_cost_records = getCurrentTrip().getCostrecords(dataSource, true);
        listView_costentries = (ListView) findViewById(R.id.listview_costentries);
        if(Util.size(list_cost_records) > 0) {
            overviewRowAdapter = new CostOverviewRowAdapter(this, list_cost_records);
            overviewRowAdapter.addOnRefreshRequestedListener(this);
            listView_costentries.setAdapter(overviewRowAdapter);
        }
        else {
            defaultAdapter = new ArrayAdapter(this, R.layout.row_simple, new String[]{"Please add a cost entry..."});
            listView_costentries.setAdapter(defaultAdapter);
        }


    }
}
