package com.example.travelapp.travelapp.Data;

import android.content.ContentValues;

import com.example.travelapp.travelapp.Helper.DictionaryHelper;

import java.util.concurrent.TimeoutException;

/**
 * Created by danilofink on 13/01/18.
 */
public class TPackinglistItem extends TBaseDbObject {

    // Columndefinition
    public static final String CN_PACKINGLIST_ID= "packinglist_id";
    public static final String CD_PACKINGLIST_ID= SCD_INT + " " + TravelAppDbHelper.References.PACKINGLIST_ID;
    public static final String CN_ITEM_ID       = "item_id";
    public static final String CD_ITEM_ID       = SCD_INT + " " + TravelAppDbHelper.References.ITEM_ID;
    public static final String CN_CHECKED       = "checked";
    public static final String CD_CHECKED       = SCD_BOOL;

    // Membervariables
    private long packinglistId;
    private long itemId;
    private TItem item;
    private boolean checked;

    @Override
    public String getTable() {
        return TravelAppDbHelper.Tables.PACKINGLIST_ITEMS;
    }

    @Override
    public Column[] getColumnDefinitions() {
        return new Column[]{
                new Column(CN_ID, CD_ID),
                new Column(CN_PACKINGLIST_ID, CD_PACKINGLIST_ID),
                new Column(CN_ITEM_ID, CD_ITEM_ID),
                new Column(CN_CHECKED, CD_CHECKED)
        };
    }

    /** Empty Constructor. */
    public TPackinglistItem() {
        setDefaultIdsAndReferences();
    }

    /** Constructor. */
    public TPackinglistItem(long packinglistId, long itemId) {
        this();
        setPackinglistId(packinglistId);
        setItemId(itemId);
    }

    @Override
    protected void setDefaultIdsAndReferences() {
        super.setDefaultIdsAndReferences();
        setPackinglistId(INVALID_ID);
        setItemId(INVALID_ID);
    }

    /** Gets the packinglist id. */
    public long getPackinglistId() {
        return packinglistId;
    }

    /** Sets the packinglist id. */
    public void setPackinglistId(long val) {
        this.packinglistId = val;
    }

    /** Gets the item id. */
    public long getItemId() {
        return itemId;
    }

    /** Sets the item id. */
    public void setItemId(long val) {
        this.itemId = val;
    }

    /** Gets the associated item object from the database or the cache. */
    public TItem getItem(TravelAppDataSource dataSource) {
        long iId = getItemId();
        if (item == null && iId != INVALID_ID) {
            item = new TItem();
            item.setId(iId);
            item = dataSource.selectObj(item);
        }
        return item;
    }

    /** Sets a new Item. But not directly in the database! */
    public void setItem(TItem val) {
        item = val;
        setItemId(val.getId());
    }
    /** Gets if the item is checked. */
    public boolean isChecked() {
        return checked;
    }

    /** Sets if the item is checked.  */
    public void setChecked(boolean value) {
        this.checked = value;
    }

    @Override
    public ContentValues getContentValues(boolean includePrimaryKey) {
        ContentValues cv = new ContentValues();
        if (includePrimaryKey) {
            cv.put(CN_ID, getId());
        }
        cv.put(CN_PACKINGLIST_ID, getPackinglistId());
        cv.put(CN_ITEM_ID       , getItemId());
        cv.put(CN_CHECKED       , isChecked());
        return cv;
    }

    @Override
    public IDatabaseObject createObjWithContentVals(ContentValues values) {
        TPackinglistItem obj = new TPackinglistItem();
        obj.setId(DictionaryHelper.getInt(values, CN_ID, INVALID_ID));
        obj.setPackinglistId(DictionaryHelper.getInt(values, CN_PACKINGLIST_ID, INVALID_ID));
        obj.setItemId(DictionaryHelper.getInt(values, CN_ITEM_ID, INVALID_ID));
        obj.setChecked(values.getAsBoolean(CN_CHECKED));
        return obj;
    }
}
